﻿using Moonlit.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem : MonoBehaviour, IMessageReceiver {

    public float _MinX;
    public float _MaxX;
    public float _MinY;
    public float _MaxY;

    public Vector2 offset;

    private void OnEnable() {
        this.Register();
    }

    private void OnDisable() {
        this.Unregister();
    }

    [MessageSlot(DispatchMethod.Subscriber, DispatchPriority.Low)]
    public void Receive(PlayerPositionChanged msg)
    {
        transform.position = 
            new Vector3(
                Mathf.Clamp(msg.position.x, _MinX, _MaxX) + offset.x,
                Mathf.Clamp(msg.position.y, _MinY, _MaxY) + offset.y,
                0);
        
    }
}
