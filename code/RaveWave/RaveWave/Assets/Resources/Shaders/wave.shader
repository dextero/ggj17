﻿Shader "FX/wave"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amplitude("Amplitude", Float) = 1.0
        _Noise("Noise", Float) = 0.1
        _Frequency("Frequency", Float) = 10.0
        _TimeScale("Time Scale", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent+1" }
        LOD 100
        
        GrabPass{ "_ScreenTex" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "perlin.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 wpos : TEXCOORD2;
                float4 grabPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _ScreenTex;
            float4 _MainTex_ST;

            float _Amplitude;
            float _Noise;
            float _Frequency;
            float _TimeScale;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex  = UnityObjectToClipPos(v.vertex);
                o.grabPos = ComputeGrabScreenPos(o.vertex);
                o.wpos = mul(unity_ObjectToWorld, v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                i.grabPos.x += sin(i.wpos.y* _Frequency * (1.0f + _Noise * fBm(i.wpos.yyy + _Time.z * _TimeScale, 8, 1.0f, 1.0f, 1.0f)) + _Time.w * _TimeScale) * _Amplitude * 0.01f;
                return  tex2Dproj(_ScreenTex, i.grabPos);
            }
            ENDCG
        }
    }
}
