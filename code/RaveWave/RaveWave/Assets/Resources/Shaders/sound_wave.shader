﻿Shader "FX/sound_wave"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Radius("Radius", Float) = 1.0
        _Width("Width", Float) = 0.2
        _Center("Center", Vector) = (0.0, 0.0,  0.0, 0.0)
        _Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
        _Force("Force", Float) = 1.0
        _Noise("Noise", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent+2" }
        LOD 100
        
        GrabPass{ "_FinalTex" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "perlin.cginc"



            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 wpos : TEXCOORD2;
                float4 grabPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _FinalTex;
            float4 _MainTex_ST;
            float _Radius;
            float _Width;
            float3 _Center;
            float4 _Color;
            float _Force;
            float _Noise;

            float _Range[8];

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex  = UnityObjectToClipPos(v.vertex);
                o.grabPos = ComputeGrabScreenPos(o.vertex);
                o.wpos = mul(unity_ObjectToWorld, v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float distance = length(i.wpos - _Center);
                float3 direction = normalize(i.wpos - _Center);
             
                float angle = atan2(_Center.y - i.wpos.y, _Center.x - i.wpos.x) + 3.1415;

                float4 color = float4(1.0f, 1.0f, 1.0f, 0.05f) * 0.05f;
                for (int it = 0; it < 4; ++it)
                {
                    if (angle > _Range[2 * it] && angle < _Range[2 * it + 1])
                    {
                        color = _Color;
                    }
                }

                float diff = (distance - _Radius) / _Width *(1.0f + _Noise * fBm(i.wpos.xyz * 10.0f + _Time.xyz, 4, 1.0f, 1.0f, 1.0f));

                float circleDistance = -cos(diff);
                if (diff > 1.0f || diff < -1.0f)
                {
                    circleDistance = 0.0f;
                }


                float4 force = circleDistance * float4(direction.x, direction.z, 0.0f, 0.0f) * 0.1f * _Force;
                return  tex2Dproj(_FinalTex, i.grabPos + force) - circleDistance * color * _Color.a;
            }
            ENDCG
        }
    }
}
