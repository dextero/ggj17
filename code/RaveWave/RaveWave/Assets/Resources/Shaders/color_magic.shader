﻿Shader "Sprites/color_magic"
{
    Properties
    {
        [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        _Color("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap("Pixel snap", Float) = 0
        [MaterialToggle] Noise("Noise", Float) = 0
        [MaterialToggle] Flicker("Flicker", Float) = 0
        _RemovedColor("Removed", Color) = (1, 1, 1, 1)
        _AddedColor("Added", Color) = (1, 1, 1, 1)
        _Threshold("Threshold", Float) = 0.5
        _Speed("Speed", Float) = 0.0
    }

        SubShader
    {
        Tags
    {
        "Queue" = "Transparent"
        "IgnoreProjector" = "True"
        "RenderType" = "Transparent"
        "PreviewType" = "Plane"
        "CanUseSpriteAtlas" = "True"
    }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
    {
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #pragma target 2.0
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ FLICKER_ON
        #pragma multi_compile _ NOISE_ON
        #include "UnityCG.cginc"
        #include "Perlin.cginc"

        struct appdata_t
    {
        float4 vertex   : POSITION;
        float4 color    : COLOR;
        float2 texcoord : TEXCOORD0;
    };

    struct v2f
    {
        float4 vertex   : SV_POSITION;
        fixed4 color : COLOR;
        float2 texcoord  : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
    };

    fixed4 _Color;

    v2f vert(appdata_t IN)
    {
        v2f OUT;
        UNITY_SETUP_INSTANCE_ID(IN);
        OUT.vertex = UnityObjectToClipPos(IN.vertex);
        OUT.texcoord = IN.texcoord;
        OUT.color = IN.color * _Color;
        #ifdef PIXELSNAP_ON
        OUT.vertex = UnityPixelSnap(OUT.vertex);
        #endif

        return OUT;
    }

    sampler2D _MainTex;
    sampler2D _AlphaTex;
    float4 _RemovedColor;
    float4 _AddedColor;
    float _Threshold;
    float _Speed;

    fixed4 SampleSpriteTexture(float2 uv)
    {
        fixed4 color = tex2D(_MainTex, uv);
        return color;
    }

    fixed4 frag(v2f IN) : SV_Target
    {
        fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;

        float multiplayer = 1.0f;

        #ifdef FLICKER_ON
        multiplayer *= (cos(_Speed * _Time.z) + 1.0f) * 0.5f;
        #endif

        #ifdef NOISE_ON
        multiplayer *= 2.5f * fBm(IN.vertex.xyz * _Time.x * 0.001f, 4, 5.0f, 1.0f, 1.0f);
        #endif

        float value = multiplayer *  saturate(1.0f - length(c.rgb - _RemovedColor));
  

        if (value < _Threshold)
        {
            c.rgb *= c.a;
            return c;
        }
        
        c.rgb = (c.rgb * (1.0f - value) + _AddedColor.rgb * value) * c.a;
      
        return c;
    }
        ENDCG
    }
    }
}
