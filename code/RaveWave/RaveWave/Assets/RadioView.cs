﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RadioView : MonoBehaviour, IMessageReceiver
{
    [Serializable]
    public class RadioViewEntry
    {
        public GameController.BeatStation Station;
        public Sprite Logo;
    }

    [SerializeField] private SpriteRenderer _Renderer;
    [SerializeField] private List<RadioViewEntry> _Entries;

    private GameController.BeatStation _CurrentStation;

    private void OnEnable()
    {
        this.Register();
    }
    private void OnDisable()
    {
        this.Unregister();
    }

    [MessageSlot]
    private void Receive(Beat beat)
    {
        if(beat.BColor == _CurrentStation)
        {
            return;
        }
        _CurrentStation = beat.BColor;

        var entry =  _Entries.Find(x => x.Station == beat.BColor);
        if(entry == null)
        {
            return;
        }
        var tweender = _Renderer.material.DOColor(Color.clear, 1.0f).OnComplete(() => {

            _Renderer.sprite = entry.Logo;
            _Renderer.material.DOColor(Color.white, 1.0f);
        });
    }

}
