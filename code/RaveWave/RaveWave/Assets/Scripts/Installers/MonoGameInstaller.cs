﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MonoGameInstaller : MonoInstaller<MonoGameInstaller> {

    private const string SCENE_ROOT = "scene_root";    
    private const string UI_ROOT = "ui_root";
    private const string UI_ROOT_TRANSITION = "ui_transition";    
    private const string SCENE_CONTROLLERS = "controllers";

    public override void InstallBindings() {
        Container.BindInstance<MonoBehaviour>(this);

        Container
            .BindFactory<GameScreen, GameScreen.Factory>()
            .FromPrefabResource("Prefabs/Screens/game_screen")
            .UnderGameObjectGroup(SCENE_ROOT);

        Container
            .BindFactory<TeamLogo, TeamLogo.Factory>()
            .FromPrefabResource("Prefabs/Screens/team_logo")
            .UnderGameObjectGroup(UI_ROOT);

        Container
            .BindFactory<Story, Story.Factory>()
            .FromPrefabResource("Prefabs/Screens/story")
            .UnderGameObjectGroup(UI_ROOT);        

        Container
            .Bind<TunerView>()
            .FromPrefabResource("Prefabs/TunerView")
            .UnderGameObjectGroup(UI_ROOT)
            .AsSingle();

        Container
         .Bind<HealthView>()
         .FromPrefabResource("Prefabs/HealthView")
         .UnderGameObjectGroup(UI_ROOT)
         .AsSingle();

        Container
            .BindFactory<MenuScreen, MenuScreen.Factory>()
            .FromPrefabResource("Prefabs/Screens/menu_screen")
            .UnderGameObjectGroup(UI_ROOT);

        Container
            .Bind<MainMenuDialScroller2D>()
            .FromPrefabResource("Prefabs/Screens/menu_screen_text_container");

        Container
            .Bind<HighScoreNameInput>()
            .FromPrefabResource("Prefabs/Screens/hiscore_input_letters_container");

        Container
            .BindFactory<Entry, HighScoreNameInputScreen, HighScoreNameInputScreen.Factory>()
            .FromPrefabResource("Prefabs/Screens/hiscore_input_screen")
            .UnderGameObjectGroup(UI_ROOT);

        Container
            .BindFactory<Entry, HighScoreScreen, HighScoreScreen.Factory>()
            .FromPrefabResource("Prefabs/Screens/hiscore_screen")
            .UnderGameObjectGroup(UI_ROOT);

        Container
            .BindFactory<Entry, HighScoreEntry, HighScoreEntry.Factory>()
            .FromPrefabResource("Prefabs/Screens/hiscore_entry");

        Container
            .Bind<AudioSystem>()
            .FromPrefabResource("Prefabs/Audio/audio_system")
            .UnderGameObjectGroup(SCENE_CONTROLLERS)
            .AsSingle()
            .NonLazy();

        Container.
            Bind<Perlin>()
            .FromPrefabResource("Prefabs/Graphics/perlin")
            .UnderGameObjectGroup(SCENE_ROOT)
            .AsSingle()
            .NonLazy();

        Container
            .BindFactory<Lighting, Lighting.Factory>()
            .FromPrefabResource("Prefabs/Graphics/lightning")
            .UnderGameObjectGroup(SCENE_ROOT);

        Container
            .Bind<ComboCounter>()
            .FromPrefabResource("Prefabs/combo_counter")
            .UnderGameObjectGroup(SCENE_ROOT)
            .AsSingle();

        Container
            .Bind<ComboMessageController>()
            .FromPrefabResource("Prefabs/combo_message")
            .UnderGameObjectGroup(UI_ROOT)
            .AsSingle();

        Container
            .Bind<Splash>()
            .FromPrefabResource("Prefabs/Screens/transition")
            .UnderGameObjectGroup(UI_ROOT_TRANSITION)
            .AsSingle()
            .NonLazy();

        Container
            .Bind<IInputHandler>()
            .To<GamepadInputHandler>()
//            .To<KeyboardInputHandler>()
            .AsSingle();
        
        Container
            .BindFactory<PlayerController, PlayerController.Factory>()
            .FromPrefabResource("Prefabs/Player");

        Container
            .BindFactory<Vector2, GameController.BeatStation, BeatWaveController, BeatWaveController.Factory>()
            .FromPrefabResource("Prefabs/BeatWave")
            .UnderGameObjectGroup(SCENE_ROOT);

        InstallEnemies();
    }

    protected void InstallEnemies() {
        Container
            .BindFactory<EnemyBase, EnemyBase.EnemyFlower>()
            .FromPrefabResource("Prefabs/Enemies/enemy_flower");

        Container
            .BindFactory<EnemyBase, EnemyBase.EnemyRound>()
            .FromPrefabResource("Prefabs/Enemies/enemy_round");

        Container
            .BindFactory<EnemyBase, EnemyBase.EnemySine>()
            .FromPrefabResource("Prefabs/Enemies/enemy_sine");

        Container
            .BindFactory<Bullet, Bullet.BasicBullet>()            
            .FromPrefabResource("Prefabs/Enemies/basic_bullet")
            .UnderGameObjectGroup(SCENE_ROOT);
    }
}