﻿using Moonlit.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour, IMessageReceiver
{
    public Image _Background;
    public Image _Health;

    public Color MaxColor = Color.green;
    public Color MediumColor = Color.yellow;
    public Color LowColor = Color.red;

    public float MaxFreq = 1.0f;
    public float MediumFreq = 3.0f;
    public float LowFreq = 7.0f;

    private float _Freq;
    private Vector3 _Scale;

    public void Awake()
    {
        _Background.color = MaxColor;
        _Health.color = MaxColor;
        _Freq = MaxFreq;

        _Scale = _Health.transform.localScale;
    }
    public void OnEnable()
    {
        this.Register();
    }
    public void OnDisable()
    {
        this.Unregister();
    }

    public void Update()
    {
        _Health.transform.localScale = new Vector3(_Scale.x, _Scale.y -  (Mathf.Sin(_Freq * Time.realtimeSinceStartup) + 0.5f) * 0.5f, _Scale.z);
    }

    [MessageSlot]
    private void Receive(PlayerDamaged msg)
    {
        var percent = msg.Current / msg.Max;

        if(percent > 0.99f)
        {
            _Background.color = MaxColor;
            _Health.color = MaxColor;
            _Freq = MaxFreq;
            return;
        }
        if(percent > 0.5f)
        {
            _Background.color = MediumColor;
            _Health.color = MediumColor;
            _Freq = MediumFreq;
            return;
        }

        _Background.color = LowColor;
        _Health.color = LowColor;
        _Freq = LowFreq;
    }
}
