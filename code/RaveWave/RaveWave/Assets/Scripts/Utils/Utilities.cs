﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Utilities {

    static InPlaceArray<Vector2> visitedNodes = new InPlaceArray<Vector2>(1000);
    static InPlaceArray<Vector2> nodesToVisit = new InPlaceArray<Vector2>(100);
    public static Vector2[] dirsArray = new Vector2[] {
        Vector2.left,
        Vector2.right,
        Vector2.up,
        Vector2.down };

    static int VecToKey(Vector2 vec) {
        return Mathf.RoundToInt(vec.x) * 10 + Mathf.RoundToInt(vec.y);
    }

    public static void SetSpriteDepth(Transform transform)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }

    public static Vector2 Sign0(this Vector2 vec) {
        return new Vector2(Sign0(vec.x), Sign0(vec.y));
    }

    public static int Sign0(float val) {
        if (val < -0.001f) return -1;
        if (val > 0.001f) return 1;
        return 0;
    }

    public static void Shuffle<T>(this List<T> list) {
        Shuffle(list, list.Count);
    }

    public static void Shuffle<T>(this List<T> list, int count) {
        int n = count;
        while (n > 1) {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static Color CopyWithAlpha(this Color c, float newAlpha) {
        return new Color(c.r, c.g, c.b, newAlpha);
    }

    public static T PickRandom<T>(this List<T> list) {
        return PickRandom(list, list.Count);
    }

    public static T PickRandom<T>(this List<T> list, int count) {
        if (count == 0) return default(T);
        return list[UnityEngine.Random.Range(0, count)];
    }

    public static T RandomEnum<T>() {
        Type type = typeof(T);
        Array values = Enum.GetValues(type);
                
        // skip the empty one
        object value = values.GetValue(UnityEngine.Random.Range(1, values.Length));
        
        return (T)Convert.ChangeType(value, type);        
    }
}

