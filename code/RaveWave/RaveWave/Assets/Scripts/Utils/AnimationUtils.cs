﻿using DG.Tweening;
using UnityEngine;

public class AnimationUtils {
    private static float accuracy = 0.000001f;

    private static Vector2 CalculateBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector2 p = uuu * p0; //first term
        p += 3 * uu * t * p1; //second term
        p += 3 * u * tt * p2; //third term
        p += ttt * p3; //fourth term

        return p;
    }

    public static EaseFunction GetBeizerEasing(float c1, float c2, float c3, float c4) {
        return delegate(float time, float duration, float overhsoot, float period) {
            float t = time / duration;

            float left = 0.0f;
            float right = 1.0f;
            Vector2 p0 = Vector2.zero;
            Vector2 p1 = new Vector2(c1, c2);
            Vector2 p2 = new Vector2(c3, c4);
            Vector2 p3 = Vector2.one;

            float center = (right + left) / 2;
            Vector2 pos = CalculateBezierPoint(center, p0, p1, p2, p3);
            float diff = pos.x - t;

            int iter = 0;
            while (Mathf.Abs(diff) > accuracy && (right - left) > accuracy) {
                if (diff > 0)
                    right = center;
                else
                    left = center;

                center = (right + left) / 2;
                pos = CalculateBezierPoint(center, p0, p1, p2, p3);
                diff = pos.x - t;
                iter++;
            }

            return pos.y;
        };
    }
}