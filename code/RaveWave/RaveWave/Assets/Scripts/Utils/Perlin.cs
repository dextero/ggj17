﻿using UnityEngine;

[ExecuteInEditMode]
public class Perlin : MonoBehaviour
{
    #region Private Variables
    private const int SIZE = 256;
    private int[] m_perm = new int[SIZE+SIZE];

    private Texture2D _PermTable1D;
    private Texture2D _PermTable2D;
    private Texture2D _Gradient2D;
    private Texture2D _Gradient3D;
    private Texture2D _Gradient4D;
    #endregion Private Variables

    #region Public Methods
    public void Seed(int seed)
    {
        Random.seed = seed;

        int i, j, k;
        for(i = 0; i < SIZE; i++)
        {
            m_perm[i] = i;
        }

        while(--i != 0)
        {
            k = m_perm[i];
            j = Random.Range(0, SIZE);
            m_perm[i] = m_perm[j];
            m_perm[j] = k;
        }

        for(i = 0; i < SIZE; i++)
        {
            m_perm[SIZE + i] = m_perm[i];
        }

    }
    public void Load()
    {
        LoadPermTable1D();
        LoadPermTable2D();

        LoadGradient2D();
        LoadGradient3D();
        LoadGradient4D();

        Shader.SetGlobalTexture("_PermTable1D", _PermTable1D);
        Shader.SetGlobalTexture("_PermTable2D", _PermTable2D);

        Shader.SetGlobalTexture("_Gradient2D", _Gradient2D);
        Shader.SetGlobalTexture("_Gradient3D", _Gradient3D);
        Shader.SetGlobalTexture("_Gradient4D", _Gradient4D);
    }
    #endregion Public Methods

    #region Unity Messages
    void Start()
    {
        Seed(0);
        Load();
    }
    #endregion Unity Messages

    #region Private Methods
    private void LoadPermTable1D()
    {
        if(_PermTable1D) return;

        _PermTable1D = new Texture2D(SIZE, 1, TextureFormat.Alpha8, false, true);
        _PermTable1D.filterMode = FilterMode.Point;
        _PermTable1D.wrapMode = TextureWrapMode.Repeat;

        for(int x = 0; x < SIZE; x++)
        {
            _PermTable1D.SetPixel(x, 1, new Color(0, 0, 0, (float)m_perm[x] / (float)(SIZE - 1)));
        }

        _PermTable1D.Apply();
    }

    private void LoadPermTable2D()
    {
        if(_PermTable2D) return;

        _PermTable2D = new Texture2D(SIZE, SIZE, TextureFormat.ARGB32, false, true);
        _PermTable2D.filterMode = FilterMode.Point;
        _PermTable2D.wrapMode = TextureWrapMode.Repeat;

        for(int x = 0; x < SIZE; x++)
        {
            for(int y = 0; y < SIZE; y++)
            {
                int A = m_perm[x] + y;
                int AA = m_perm[A];
                int AB = m_perm[A + 1];

                int B =  m_perm[x + 1] + y;
                int BA = m_perm[B];
                int BB = m_perm[B + 1];

                _PermTable2D.SetPixel(x, y, new Color((float)AA / 255.0f, (float)AB / 255.0f, (float)BA / 255.0f, (float)BB / 255.0f));
            }
        }

        _PermTable2D.Apply();
    }

    private void LoadGradient2D()
    {
        if(_Gradient2D) return;

        _Gradient2D = new Texture2D(8, 1, TextureFormat.RGB24, false, true);
        _Gradient2D.filterMode = FilterMode.Point;
        _Gradient2D.wrapMode = TextureWrapMode.Repeat;

        for(int i = 0; i < 8; i++)
        {
            float R = (GRADIENT2[i*2+0] + 1.0f) * 0.5f;
            float G = (GRADIENT2[i*2+1] + 1.0f) * 0.5f;

            _Gradient2D.SetPixel(i, 0, new Color(R, G, 0, 1));
        }

        _Gradient2D.Apply();

    }
    private void LoadGradient3D()
    {
        if(_Gradient3D) return;

        _Gradient3D = new Texture2D(SIZE, 1, TextureFormat.RGB24, false, true);
        _Gradient3D.filterMode = FilterMode.Point;
        _Gradient3D.wrapMode = TextureWrapMode.Repeat;

        for(int i = 0; i < SIZE; i++)
        {
            int idx = m_perm[i] % 16;

            float R = (GRADIENT3[idx*3+0] + 1.0f) * 0.5f;
            float G = (GRADIENT3[idx*3+1] + 1.0f) * 0.5f;
            float B = (GRADIENT3[idx*3+2] + 1.0f) * 0.5f;

            _Gradient3D.SetPixel(i, 0, new Color(R, G, B, 1));
        }

        _Gradient3D.Apply();

    }
    private void LoadGradient4D()
    {
        if(_Gradient4D) return;

        _Gradient4D = new Texture2D(SIZE, 1, TextureFormat.ARGB32, false, true);
        _Gradient4D.filterMode = FilterMode.Point;
        _Gradient4D.wrapMode = TextureWrapMode.Repeat;

        for(int i = 0; i < SIZE; i++)
        {
            int idx = m_perm[i] % 32;

            float R = (GRADIENT4[idx*4+0] + 1.0f) * 0.5f;
            float G = (GRADIENT4[idx*4+1] + 1.0f) * 0.5f;
            float B = (GRADIENT4[idx*4+2] + 1.0f) * 0.5f;
            float A = (GRADIENT4[idx*4+3] + 1.0f) * 0.5f;

            _Gradient4D.SetPixel(i, 0, new Color(R, G, B, A));
        }

        _Gradient4D.Apply();
    }
    #endregion Private Methods

    #region Gradient Tables
    static float[] GRADIENT2 = new float[]
    {
        0, 1,
        1, 1,
        1, 0,
        1, -1,
        0, -1,
        -1, -1,
        -1, 0,
        -1, 1,
    };

    static float[] GRADIENT3 = new float[]
    {
        1,1,0,
        -1,1,0,
        1,-1,0,
        -1,-1,0,
        1,0,1,
        -1,0,1,
        1,0,-1,
        -1,0,-1,
        0,1,1,
        0,-1,1,
        0,1,-1,
        0,-1,-1,
        1,1,0,
        0,-1,1,
        -1,1,0,
        0,-1,-1,
    };

    static float[] GRADIENT4 = new float[]
    {
        0, -1, -1, -1,
        0, -1, -1, 1,
        0, -1, 1, -1,
        0, -1, 1, 1,
        0, 1, -1, -1,
        0, 1, -1, 1,
        0, 1, 1, -1,
        0, 1, 1, 1,
        -1, -1, 0, -1,
        -1, 1, 0, -1,
        1, -1, 0, -1,
        1, 1, 0, -1,
        -1, -1, 0, 1,
        -1, 1, 0, 1,
        1, -1, 0, 1,
        1, 1, 0, 1,

        -1, 0, -1, -1,
        1, 0, -1, -1,
        -1, 0, -1, 1,
        1, 0, -1, 1,
        -1, 0, 1, -1,
        1, 0, 1, -1,
        -1, 0, 1, 1,
        1, 0, 1, 1,
        0, -1, -1, 0,
        0, -1, -1, 0,
        0, -1, 1, 0,
        0, -1, 1, 0,
        0, 1, -1, 0,
        0, 1, -1, 0,
        0, 1, 1, 0,
        0, 1, 1, 0,
    };
    #endregion Gradient Tables
}