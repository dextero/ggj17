﻿//////////////////////////////////////////////////////////////////////////
// Moonlit.Messages v0.1.2:
// 
// Namespaces:
// 
// - Moonlit.Messages:       main plug-in namespace
// - Moonlit.Messages.Demos: demo scripts 
// - Moonlit.Messages.Tools: editor windows, custom inspectors, tooling
// - Moonlit.Messages.Tests: unit/performance tests
// 
// Todos:
// 
// - ReceiverInspectorWindow.DrawComponentMethod: change Register to Bind
// 
//////////////////////////////////////////////////////////////////////////

namespace Moonlit.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    /// Routes all messages
    public static class MessageDispatcher
    {
        #region Private Types
        // Per registered receiver data
        public struct ReceiverData
        {
            public Delegate Method;

            public DispatchMethod DispatchType;
            public DispatchPriority Priority;
        }

        // Per cached method data
        public struct MethodData
        {
            public MethodInfo Info;
            public Type MessageType;
            public DispatchMethod DispatchType;
            public DispatchPriority Priority;
        }

        public delegate void Callback(MessageCallback data);
        #endregion Private Types

        #region Public Types
        public struct MessageCallback
        {
            #region Public Variables
            public object Message;

            public object Sender;
            public object Receiver;
            #endregion Public Variables
        }
        #endregion Public Types

        #region Private Variables
        // <message type, list of receiver methods to call>
        public static Dictionary<Type, List<ReceiverData>> _Bindings =
            new Dictionary<Type, List<ReceiverData>>();

        // <receiver object, list of observed senders>
        public static Dictionary<object, List<object>> _Observers =
            new Dictionary<object, List<object>>();

        // <receiver class, list of event_methods associated with that type>
        private static Dictionary<Type, List<MethodData>> _MethodCache =
            new Dictionary<Type, List<MethodData>>();

        private static bool _Consumed = false;
        #endregion Private Variables

        #region Public Variables
        public static event Callback Callbacks;
        #endregion Public Variables

        #region Internal Variables
        // Get cached methods for class
        public static List<MethodData> QueryCache<T>(T classType)
        {
            var type = classType.GetType();

            if(_MethodCache == null)
            {
                return null;
            }

            if(!_MethodCache.ContainsKey(type))
            {
                CacheTypeMethods(type);
                if(!_MethodCache.ContainsKey(type))
                {
                    return null;
                }
            }

            return _MethodCache[type];
        }
        #endregion Internal Variables

        #region Public Methods
        // Add method to dispatcher
        public static void Bind<T>(Action<T> method, DispatchMethod dispatch = DispatchMethod.Subscriber,
            DispatchPriority priority = DispatchPriority.Default)
        {
            var messageType = typeof(T);

            var target      = method.Target;
            var methodInfo  = method.Method;
            var classType   = method.Method.DeclaringType;

            ReceiverData data;
            data.Method = CreateDelegate<T>(methodInfo, target);

            data.Priority = priority;
            data.DispatchType = dispatch;

            RegisterMethod(messageType, data);
        }

        // Remove method from dispatcher
        public static void Unbind<T>(Action<T> method)
        {
            var messageType = typeof(T);
            var methodInfo  = method.Method;

            UnregisterMethod(messageType, methodInfo);
        }

        // Register class object for listening
        public static void Register<T>(T receiver) where T : class
        {
            // typeof(T) can't be used because invoking this method
            // in base class with [this] parameter would not deduce derived type
            var classType = receiver.GetType();
            CacheTypeMethods(classType);

            if(!_MethodCache.ContainsKey(classType))
            {
                return;
            }

            var methodCache = _MethodCache[classType];
            for(int i = 0; i < methodCache.Count; ++i)
            {
                var method = methodCache[i];

                ReceiverData data;
                data.Method = CreateDelegate<T>(method.Info, receiver);

                data.Priority = method.Priority;
                data.DispatchType = method.DispatchType;

                RegisterMethod(method.MessageType, data);
            }
        }

        // Unregister class  object
        public static void Unregister<T>(T receiver) where T : class
        {
            // typeof(T) can't be used because invoking this method
            // in base class with [this] parameter would not deduce derived type
            var classType = receiver.GetType();
            if(!_MethodCache.ContainsKey(classType))
            {
                return;
            }

            var methodCache = _MethodCache[classType];
            for(int i = 0; i < methodCache.Count; ++i)
            {
                var messageType = methodCache[i].MessageType;
                UnregisterAllMethods(messageType, receiver);
            }
        }

        public static void Observe(object receiver, object observed)
        {
            if(!_Observers.ContainsKey(receiver))
            {
                _Observers[receiver] = new List<object>();
            }
            _Observers[receiver].Add(observed);
        }
        public static void Unobserve(object receiver, object observed)
        {
            if(!_Observers.ContainsKey(receiver))
            {
                return;
            }
            _Observers[receiver].Remove(observed);
        }
        public static void UnobserveAll(object receiver)
        {
            if(!_Observers.ContainsKey(receiver))
            {
                return;
            }
            _Observers[receiver].Clear();
        }

        public static void Signal<T>(T message, object sender) where T : class
        {
            var messageType = typeof(T);
            if(!_Bindings.ContainsKey(messageType))
            {
                return;
            }

            var bindings = _Bindings[messageType];
            for (int i = bindings.Count - 1; i >= 0; --i)
            {
                var receiverData = bindings[i];

                if(receiverData.DispatchType == DispatchMethod.Observer)
                {
                    var receiver = receiverData.Method.Target;
                    var observed = GetObserved(receiver);

                    if(observed == null || !observed.Contains(sender))
                    {
                        continue;
                    }
                }

                ((Action<T>)receiverData.Method)(message);
                if(Callbacks != null)
                {
                    MessageCallback callbackData;
                    callbackData.Message = message;

                    callbackData.Sender  = sender;
                    callbackData.Receiver = receiverData.Method.Target;

                    Callbacks.Invoke(callbackData);
                }

                if(_Consumed)
                {
                    _Consumed = false;
                    return;
                }
            }
        }
        public static void DirectSignal(object message, object receiver)
        {
            var bindings =_Bindings[message.GetType()];
            for(int i = 0; i < bindings.Count; ++i)
            {
                var receiverData = bindings[i];

                if(bindings[i].Method.Target != receiver)
                {
                    continue;
                }
                receiverData.Method.DynamicInvoke(message);
            }
        }

        public static void Clear()
        {
            _Bindings.Clear();
            _MethodCache.Clear();
            _Observers.Clear();
        }

        public static void Consume()
        {
            _Consumed = true;
        }
        #endregion Public Methods

        #region Private Methods
        private static void RegisterMethod(Type messageType, ReceiverData data)
        {
            if(!_Bindings.ContainsKey(messageType))
            {
                _Bindings[messageType] = new List<ReceiverData> { };
            }

            _Bindings[messageType].Add(data);
            _Bindings[messageType] = _Bindings[messageType].OrderBy(x => x.Priority).ToList(); // [PERF]!
        }
        private static void UnregisterMethod(Type messageType, MethodInfo methodInfo)
        {
            if(!_Bindings.ContainsKey(messageType))
            {
                return;
            }

            var bindings = _Bindings[messageType];
            bindings.RemoveAll(x => {
                return x.Method.Method == methodInfo;
            });
        }
        private static void UnregisterAllMethods(Type messageType, object target)
        {
            if(!_Bindings.ContainsKey(messageType))
            {
                return;
            }

            _Bindings[messageType].RemoveAll(x => x.Method.Target == target);
        }

        public static void CacheTypeMethods(Type type)
        {
            if(_MethodCache.ContainsKey(type))
            {
                return;
            }

            var methods = type.GetMethods(BindingFlags.Instance | BindingFlags.FlattenHierarchy |
          BindingFlags.Public | BindingFlags.NonPublic);

            for(int i = 0; i < methods.Length; ++i)
            {
                var method = methods[i];

                //a plain old method
                if(method.IsAbstract || method.IsGenericMethod || method.IsConstructor)
                {
                    continue;
                }

                //with only one parameter
                var parameters = method.GetParameters();
                if(parameters.Length != 1)
                {
                    continue;
                }
                var messageType = parameters[0].ParameterType;

                //that has MessageSlot attribute
                var attributes = method.GetCustomAttributes(typeof(MessageSlotAttribute), true) as MessageSlotAttribute[];
                if(attributes.Length != 1)
                {
                    continue;
                }

                MethodData methodData;
                methodData.Info = method;
                methodData.MessageType = messageType;
                methodData.DispatchType = attributes[0].Type;
                methodData.Priority = attributes[0].Priority;

                if(!_MethodCache.ContainsKey(type))
                {
                    _MethodCache[type] = new List<MethodData>();
                }
                _MethodCache[type].Add(methodData);
            }
        }
        #endregion Private Methods

        #region Helper Methods
        private static Delegate CreateDelegate<T>(MethodInfo methodInfo, object target)
        {
            Type delegateType;

            var typeArgs = methodInfo.GetParameters()
                .Select(p => p.ParameterType)
                .ToList();

            // builds a delegate type
            if(methodInfo.ReturnType == typeof(void))
            {
                delegateType = Expression.GetActionType(typeArgs.ToArray());
            } else
            {
                typeArgs.Add(methodInfo.ReturnType);
                delegateType = Expression.GetFuncType(typeArgs.ToArray());
            }

            // creates a binded delegate if target is supplied
            var result = (target == null)
                ? Delegate.CreateDelegate(delegateType, methodInfo)
                : Delegate.CreateDelegate(delegateType, target , methodInfo);

            return result;
        }
        public static List<object> GetObserved<T>(T receiver)
        {
            if(receiver == null)
            {
                return null;
            }

            if(!_Observers.ContainsKey(receiver))
            {
                return null;
            }

            return _Observers[receiver];
        }
        #endregion Helper Methods
    }
}