﻿
namespace Moonlit.Messages
{
    // Shortcut for creating static messages for removing dynamic memory allocation
    public class DefaultMessage<T> where T : class, new()
    {
        public static T Default = new T();
    }
}
