﻿namespace Moonlit.Messages
{
    // Base receiver interface, used to hook up extensions methods
    public interface IMessageReceiver { }

    public static partial class Extentions
    {
        #region Public Methods
        // Adds/removes observed objects for [DispatchMethod.Observer] methods
        public static void AddObserved(this IMessageReceiver rec, object obj)
        {
            MessageDispatcher.Observe(rec, obj);
        }
        public static void RemoveObserved(this IMessageReceiver rec, object obj)
        {
            MessageDispatcher.Unobserve(rec, obj);
        }

        public static void Register(this IMessageReceiver rec)
        {
            MessageDispatcher.Register(rec);
        }
        public static void Unregister(this IMessageReceiver rec)
        {
            MessageDispatcher.Unregister(rec);
        }

        // Invoking Consume() on received message prevents from further propagating
        public static void Consume<T>(this T obj) where T : class
        {
            MessageDispatcher.Consume();
        }
        #endregion Public Methods
    }
}