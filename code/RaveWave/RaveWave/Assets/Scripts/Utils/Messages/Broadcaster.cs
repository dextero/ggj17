﻿namespace Moonlit.Messages
{
    using UnityEngine;

    // Simplifies connection between observed/receiving objects
    class Broadcaster : MonoBehaviour
    {
        #region Inspector Variables
        [SerializeField]
        private Component _Observed;
        [SerializeField]
        private Component _Receiver;
        #endregion Inspector Variables

        #region Unity Messages
        private void Awake()
        {
            if(!IsValid())
            {
                enabled = false;
            }
        }
        private void OnEnable()
        {
            if(!IsValid())
            {
                return;
            }

            MessageDispatcher.Observe(_Receiver, _Observed);
        }
        private void OnDisable()
        {
            if(!IsValid())
            {
                return;
            }

            MessageDispatcher.Unobserve(_Receiver, _Observed);
        }
        #endregion Unity Messages

        #region Private Methods
        private bool IsValid()
        {
            return _Receiver != null && _Observed != null;
        }
        #endregion Private Methods
    }
}