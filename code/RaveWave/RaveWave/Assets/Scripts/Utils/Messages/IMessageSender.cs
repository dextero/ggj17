﻿namespace Moonlit.Messages
{
    // Base sender interface, used to hook up extensions methods
    public interface IMessageSender { }

    public static partial class Extentions
    {
        // Signals new message
        public static void Signal<T>(this IMessageSender obj, T message) where T : class
        {
            MessageDispatcher.Signal(message, obj);
        }

        public static void Signal<T>(this IMessageSender obj) where T : class, new()
        {
            MessageDispatcher.Signal(new T(), obj);
        }
    }
}