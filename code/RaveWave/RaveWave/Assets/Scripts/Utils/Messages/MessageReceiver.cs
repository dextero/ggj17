﻿namespace Moonlit.Messages
{
    using UnityEngine;

    // Utility class, auto-register/unregister
    public class MessageReceiver : MonoBehaviour, IMessageReceiver
    {
        #region Unity Messages
        protected virtual void OnEnable()
        {
            MessageDispatcher.Register(this);
        }

        protected virtual void OnDisable()
        {
            MessageDispatcher.Unregister(this);
        }
        #endregion Unity Messages
    }
}