﻿

namespace Moonlit.Messages
{
    using System;

    #region Public Types
    public enum DispatchMethod
    {
        Unknown = -1,
        // Publisher-Subscriber pattern, all methods with [DispatchType.Subscriber]
        // are invoked when specified Message is send
        Subscriber,

        // Observer pattern, receiver methods with [DispatchType.Observer]
        // are called on objects that observe Sender
        Observer,
    }

    public enum DispatchPriority
    {
        High = -1,
        Default = 0,
        Low = 1,
    }
    #endregion Public Types

    // Specifies that method should receive messages
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class MessageSlotAttribute : Attribute
    {
        #region Private Variables
        private DispatchMethod _Type;
        private DispatchPriority _Priority;
        #endregion Private Variables

        #region Public Variables
        public DispatchMethod Type
        {
            get { return _Type; }
        }
        public DispatchPriority Priority
        {
            get
            {
                return _Priority;
            }
        }

        public Enum Tag { get; set; }
        #endregion Public Variables

        #region Public Methods
        public MessageSlotAttribute(DispatchMethod type = DispatchMethod.Subscriber,
            DispatchPriority priority = DispatchPriority.Default)
        {
            _Type = type;
            _Priority = priority;
        }
        #endregion Public Methods
    }
}