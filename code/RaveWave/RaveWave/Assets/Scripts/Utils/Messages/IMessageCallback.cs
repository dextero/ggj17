﻿namespace Moonlit.Messages
{
    // Base callback interface, used to hook up extensions methods
    public interface IMessageCallback { }

    public static partial class Extentions
    {
        #region Public Methods
        // Adds/removes callback methods
        public static void AddCallback(this IMessageCallback obj, MessageDispatcher.Callback callback)
        {
            MessageDispatcher.Callbacks += callback;
        }
        public static void RemoveCallback(this IMessageCallback obj, MessageDispatcher.Callback callback)
        {
            MessageDispatcher.Callbacks -= callback;
        }
        #endregion Public Methods
    }
}