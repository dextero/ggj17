﻿using System;
using System.Linq;
using DG.Tweening;
using ModestTree.Util;
using Moonlit.Messages;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ComboCounter: MonoBehaviour, IMessageReceiver, IMessageSender {
    #region Configuration
    [SerializeField] protected float _ComboTimeLimitSeconds = 3.0f;

    [SerializeField] protected float _ComboPopupFadeInTimeSeconds = 0.2f;
    [SerializeField] protected float _ComboPopupDisplayTimeSeconds = 3.0f;
    [SerializeField] protected float _ComboPopupFadeOutTimeSeconds = 0.2f;

    [SerializeField] protected float _ComboPopupBeatInTimeSeconds = 0.15f;
    [SerializeField] protected float _ComboPopupBeatOutTimeSeconds = 0.15f;
    [SerializeField] protected float _ComboPopupBeatScale = 1.2f;
    #endregion

    #region Message types
    [Serializable]
    public class Hit {}
    [Serializable]
    public class EnemyKilled { }
    #endregion

    [Inject] private ComboMessageController comboMessage;

    private int comboHitCounter = 0;
    private float lastHitTime = 0.0f;

    void Start() {
        this.Register();
    }

    void OnDestroy() {
        this.Unregister();
    }

    [MessageSlot]
    public void OnHit(Hit hit) {
        ++comboHitCounter;
        this.Signal(new ScoreGained(1 * comboHitCounter * comboHitCounter));
        lastHitTime = Time.time;
    }

    [MessageSlot]
    void OnBeat(Beat beat) {
        if (beat.Type == AudioChannel.BeatType.None) return;
        if (comboMessage && comboMessage.gameObject.activeInHierarchy) {
            DOTween.Sequence()
                   .SetId("combo_beat_tween")
                   .Append(comboMessage.transform.DOScale(new Vector3(_ComboPopupBeatScale, _ComboPopupBeatScale), _ComboPopupBeatInTimeSeconds))
                   .Append(comboMessage.transform.DOScale(new Vector3(1.0f, 1.0f), _ComboPopupBeatOutTimeSeconds));
        }
    }

    [MessageSlot]
    void OnGameOver(GameOver gameOver) {
        // do not handle any more messages
        this.Unregister();
        comboHitCounter = 0;
    }

    private void CancelTweens() {
        DOTween.Kill("combo_tween");
        DOTween.Kill("combo_beat_tween");
    }

    private string ChooseImage(int combo) {
        Tuple<int, string>[] values = new[] {
            new Tuple<int, string>(  5, "Textures/tp_nice"),
            new Tuple<int, string>( 10, "Textures/tp_sweet"),
            new Tuple<int, string>( 20, "Textures/tp_super"),
            new Tuple<int, string>( 50, "Textures/tp_awesome"),
            new Tuple<int, string>(100, "Textures/tp_fantastic"),
            new Tuple<int, string>(200, "Textures/tp_radical"),
            new Tuple<int, string>(500, "Textures/tp_booyah"),
        };

        for (int i = 0; i < values.Length; ++i) {
            if (combo < values[i].First) {
                return values[i].Second;
            }
        }

        return values.Last().Second;
    }

    void Update() {
        if (comboMessage && comboHitCounter > 0 && Time.time - lastHitTime > _ComboTimeLimitSeconds) {
            CancelTweens();

            comboMessage.Image.sprite = Resources.Load<Sprite>(ChooseImage(comboHitCounter));
            comboMessage.transform.localScale = new Vector3(0.0f, 0.0f);

            DOTween.Sequence()
                   .SetId("combo_tween")
                   .AppendCallback(() => comboMessage.gameObject.SetActive(true))
                   .Append(comboMessage.transform.DOScale(new Vector3(1.0f, 1.0f), _ComboPopupFadeInTimeSeconds))
                   .AppendInterval(_ComboPopupDisplayTimeSeconds)
                   .AppendCallback(() => DOTween.Kill("combo_beat_tween"))
                   .Append(comboMessage.transform.DOScale(new Vector3(0.0f, 0.0f), _ComboPopupFadeOutTimeSeconds))
                   .AppendCallback(() => comboMessage.gameObject.SetActive(false));

            comboHitCounter = 0;
        }
    }
}
