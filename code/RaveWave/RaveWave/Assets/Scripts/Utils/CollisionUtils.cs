﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CollisionUtils {
    protected static InPlaceArray<Vector2> _PositionArr = new InPlaceArray<Vector2>(100);

    public static bool IsGridPositionInRange(Vector2 position) {
        if (position.x < 0 || position.y < 0) return false;
        if (position.x >= 100 || position.y >= 46) return false;
        return true;
    }

    public static bool IsCircleInRange(Vector2 position, float radius) {
        return
            IsGridPositionInRange(new Vector2(position.x - radius, position.y)) &&
            IsGridPositionInRange(new Vector2(position.x + radius, position.y)) &&
            IsGridPositionInRange(new Vector2(position.x, position.y - radius)) &&
            IsGridPositionInRange(new Vector2(position.x, position.y + radius));
    }

    /*
     * 0 - no collision
     * 1 - horizontal collide
     * 2 - vertical collide     
     */
    public static int CircleInRectRange(Vector2 position, float radius) {
        if (!IsGridPositionInRange(new Vector2(position.x - radius, position.y)) ||
            !IsGridPositionInRange(new Vector2(position.x + radius, position.y)))
            return 1;
        if(!IsGridPositionInRange(new Vector2(position.x, position.y - radius)) ||
           !IsGridPositionInRange(new Vector2(position.x, position.y + radius)))
            return 2;
        return 0;
    }

    public static bool CircleRectIntersection(Vector2 circleCenter, float circleRadius, Rect rect) {        
        // Find the closest point to the circle within the rectangle
        float closestX = Mathf.Clamp(circleCenter.x, rect.xMin, rect.xMax);
        float closestY = Mathf.Clamp(circleCenter.y, rect.yMin, rect.yMax);

        // Calculate the distance between the circle's center and this closest point
        float distanceX = circleCenter.x - closestX;
        float distanceY = circleCenter.y - closestY;

        // If the distance is less than the circle's radius, an intersection occurs
        float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
        return distanceSquared < (circleRadius * circleRadius);
    }

    public static Rect CircleRectCheck(Vector2 circleCenter, float circleRadius) {
        return new Rect(
            Mathf.Floor(circleCenter.x - circleRadius),
            Mathf.Floor(circleCenter.y - circleRadius),
            Mathf.Ceil(circleCenter.x + circleRadius) - Mathf.Floor(circleCenter.x - circleRadius),
            Mathf.Ceil(circleCenter.y + circleRadius) - Mathf.Floor(circleCenter.y - circleRadius));
    }

    public static InPlaceArray<Vector2> CircleOccupyGrid(Vector2 circleCenter, float circleRadius) {
        _PositionArr.Clear();

        Rect searchArea = CircleRectCheck(circleCenter, circleRadius);
        for (int i = 0; i < searchArea.width; i++)
            for (int j = 0; j < searchArea.height; j++) {
                Vector2 rectPos = new Vector2(i + searchArea.x, j + searchArea.y);
                if (!IsGridPositionInRange(rectPos)) continue;

                Rect testRect = new Rect(rectPos.x, rectPos.y, 1, 1);

                if (CircleRectIntersection(circleCenter, circleRadius, testRect))
                    _PositionArr.Add(rectPos);                    
            }
        
        return _PositionArr;
    }

    public static bool DoesCircleIntersect(
        Vector2 circle1Pos, float circle1Radius, 
        Vector2 circle2Pos, float circle2Radius) {
        return (circle2Pos - circle1Pos).magnitude < circle1Radius + circle2Radius;
    }
}

