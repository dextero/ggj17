﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class InPlaceArray<T> : IComparer<T> {
    int _Count = 0;
    List<T> _Arr;
    private Func<T, T, int> _CmpLambda;

    public int Count {
        get { return _Count; }
        private set { }
    }

    public InPlaceArray() {
        _Arr = new List<T>();
    }

    public InPlaceArray(int newSize)
        : this() {
        EnsureSize(newSize);
    }

    public void EnsureSize(int newSize) {
        while (_Arr.Count() < newSize)
            _Arr.Add(default(T));
        Clear();
    }

    public void Clear() {
        _Count = 0;
    }

    public void ClearAndClean() {
        Clear();
        for (int i = 0; i < _Arr.Count(); i++)
            _Arr[i] = default(T);
    }

    public T Get(int pos) {
        if (pos < 0 || pos >= _Count)
            throw new IndexOutOfRangeException();

        return _Arr[pos];
    }

    public void Set(int pos, T value) {
        if (pos < 0 || pos >= _Count)
            throw new IndexOutOfRangeException();

        _Arr[pos] = value;
    }

    public void Reverse() {
        _Arr.Reverse(0, _Count);
    }

    public bool Contains(T value) {
        int foundAt = _Arr.IndexOf(value);
        return foundAt >= 0 && foundAt < _Count;
    }

    public void Remove(T value) {
        int foundAt = _Arr.IndexOf(value);
        if (foundAt >= 0 && foundAt < _Count)
            RemoveAt(foundAt);
    }

    public void Sort(Func<T, T, int> compare) {
        _CmpLambda = compare;
        _Arr.Sort(0, _Count, this);
        _CmpLambda = null;
    }

    public void Add(T value) {
        while (_Count + 1 >= _Arr.Count)
            _Arr.Add(default(T));        

        _Arr[_Count++] = value;
    }

    public void AddRange(InPlaceArray<T> arr) {
        for (int i = 0; i < arr.Count; i++)
            Add(arr.Get(i));
    }

    public void Insert(int pos, T value) {
        while (_Count <= pos)
            _Arr[_Count++] = default(T);
        _Arr[pos] = value;
    }

    public T PickRandom() {
        return _Arr.PickRandom<T>(_Count);
    }

    public void CopyFrom(InPlaceArray<T> otherArr) {
        Clear();
        for (int i = 0; i < otherArr.Count; i++)
            Add(otherArr.Get(i));
    }

    public void CopyFrom(List<T> otherArr) {
        Clear();
        for (int i = 0; i < otherArr.Count; i++)
            Add(otherArr[i]);
    }

    public void Shuffle() {
        _Arr.Shuffle<T>(_Count);
    }

    public void RemoveAt(int pos) {
        if (pos < 0 || pos >= Count)
            throw new IndexOutOfRangeException();

        _Count--;
        for (int i = pos; i < _Count; i++)
            _Arr[i] = _Arr[i + 1];
    }

    public int Compare(T x, T y) {
        return _CmpLambda(x, y);
    }
}

