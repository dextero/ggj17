﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    [SerializeField] private SpriteRenderer _View;

    [SerializeField] private Sprite _Left;
    [SerializeField] private Sprite _Right;
    [SerializeField] private Sprite _Up;
    [SerializeField] private Sprite _Down;
    [SerializeField]
    private Sprite Damaged;
    [SerializeField]
    private Sprite Tumbstone;

    private PlayerController _Contoller;

    WobblyWobblyWalking walking;

    private void Awake() {
        _Contoller = GetComponent<PlayerController>();
        walking = GetComponent<WobblyWobblyWalking>();
    }

    private void Update() {
        if (_Contoller.CurrHp <= 0) {
            _View.sprite = Tumbstone;
            _View.color = Color.white;

            return;
        }
        if (_Contoller._GhostTime > 0.0f) {
            walking.enabled = false;
            _View.sprite = Damaged;
            float sin = Mathf.Sin(Mathf.PI * _Contoller._GhostTime * 15.0f);            

            _View.color = new Color(
                1.0f,
                1.0f,
                1.0f, 
                Mathf.Max(0.0f, Mathf.Sign(sin)));
            
            return;
        } else {
            walking.enabled = true;
            _View.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

        var dir = _Contoller.Direction;

        if(Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
        {
            _View.sprite = dir.x > 0.0f ? _Right : _Left;

         
        } else
        {
            _View.sprite = dir.y > 0.0f ? _Up : _Down;
        }
    }

}
