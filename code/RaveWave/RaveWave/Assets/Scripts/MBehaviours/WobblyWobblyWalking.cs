﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobblyWobblyWalking : MonoBehaviour
{
    [SerializeField]
    private Transform _Graphics;

    [SerializeField]
    private float _Angle = 15.0f;
    [SerializeField]
    private float _Frequency = 150.0f;

    [SerializeField]
    private float _Current = 0.0f;

    [SerializeField]
    private AnimationCurve _Height;
    [SerializeField]
    private float _HeightScale = 0.1f;

    [SerializeField]
    private float _Threshold = 0.05f;

    private float _HeightValue = 0.0f;
    private float _Multiplayer = 1.0f;
    private Vector3 _PreviousPosition;


    private Vector3 _CacheLocalPosition;
    private Vector3 _CacheLocalRotation;

    private bool _Start = true;

    private void Awake()
    {
        _CacheLocalPosition = _Graphics.localPosition;
        _CacheLocalRotation = _Graphics.localRotation.eulerAngles;
    }

    private void Update()
    {
        if(_Start)
        {
            _PreviousPosition = transform.position;
            _Start = false;
            return;
        }

        var mag = (transform.position - _PreviousPosition).magnitude * 100.0f;
        var velocity = mag / Time.deltaTime ;

        _PreviousPosition = transform.position;

        if(mag <= _Threshold)
        {
            _Current *= 0.9f;
            _HeightValue *= 0.9f;

            _Graphics.localRotation = Quaternion.Euler(_CacheLocalRotation.x, _CacheLocalRotation.y, _CacheLocalRotation.z + _Current);
            _Graphics.localPosition = _CacheLocalPosition + new Vector3(0.0f, _HeightValue, 0.0f);
            return;
        }

        _Current += velocity * _Frequency * _Multiplayer * Time.deltaTime;
        if(_Current > _Angle)
        {
            _Multiplayer = -1.0f;
        }
        if(_Current < -_Angle)
        {
            _Multiplayer = 1.0f;
        }

        _PreviousPosition = transform.position;

        float percentage = Mathf.Clamp01(Mathf.Abs(_Current / _Angle));
        _HeightValue = _Height.Evaluate(percentage) * _HeightScale;

        _Graphics.localRotation = Quaternion.Euler(_CacheLocalRotation.x , _CacheLocalRotation.y, _CacheLocalRotation.z + _Current);
        _Graphics.localPosition = _CacheLocalPosition +  new Vector3(0.0f, _HeightValue, 0.0f);
    }
}
