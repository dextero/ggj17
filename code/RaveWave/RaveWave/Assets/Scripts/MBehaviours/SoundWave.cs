﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class SoundWave : MonoBehaviour
{
    #region Inspector Variables
    [SerializeField] private SpriteRenderer _Renderer;
    private Color _color;


    // IN DEGREES
    public Vector2[] _Ranges;
    #endregion

    private float[] _ArrayInRadians = new float[8];

    #region Public Variables
    private SpriteRenderer Renderer
    {
        get
        {
            if(_Renderer == null)
            {
                _Renderer = GetComponent<SpriteRenderer>();
            }
            return _Renderer;
        }
    }
    public float Radius
    {
        set
        {
            Renderer.sharedMaterial.SetFloat("_Radius", value);
        }
    }
    public Color Color
    {
        set
        {
            _color = value;
            Renderer.sharedMaterial.SetColor("_Color", _color);
        }
        get {
            return _color;
        }
    }
    public float Width
    {
        set
        {
            Renderer.sharedMaterial.SetFloat("_Width", value);
        }
    }
    #endregion

    private void Awake()
    {
        _ArrayInRadians = new float[8];
    }
    #region Unity Messages
    private void Update()
    {
        Renderer.sharedMaterial.SetVector("_Center", transform.position);

        for(int i = 0; i < 4; ++i)
        {
            _ArrayInRadians[2 * i]     = _Ranges[i].x / 180.0f * 3.1415f;
            _ArrayInRadians[2 * i + 1] = _Ranges[i].y / 180.0f * 3.1415f; 
        }

        Renderer.sharedMaterial.SetFloatArray("_Range", _ArrayInRadians);
    }
    #endregion 
}
