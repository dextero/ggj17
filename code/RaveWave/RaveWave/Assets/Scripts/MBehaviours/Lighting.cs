﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Lighting : GameEntity, IMessageSender
{
    
    [SerializeField] private SpriteRenderer _Renderer;
    [SerializeField] private List<Sprite> _Sprites;
    [SerializeField] private float _DeltaTime;

    private float _Time = 0.0f;

    public Action Calback;

    [SerializeField]
    AudioClip lightningSound;

    public void Awake()
    {
        _Time = UnityEngine.Random.Range(0.0f, _DeltaTime * 0.2f);

        PlaySound sound = new PlaySound();
        sound.Clip = lightningSound;
        this.Signal<PlaySound>(sound);
    }

    public void Update()
    {
        int frame = (int)(_Time / _DeltaTime);
        if(frame >= _Sprites.Count)
        {
            if (Calback != null)
                Calback();

            DestroyImmediate(this.gameObject);            
            return;
        }

        _Renderer.sprite = _Sprites[frame];
        _Time += Time.deltaTime;
    }

    public class Factory : Factory<Lighting> { };
}
