﻿using Moonlit.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour, IMessageSender
{

    [SerializeField]
    private SpriteRenderer _View;

    [SerializeField]
    AudioClip deathClip;

    [SerializeField]
    private Sprite _Left;
    [SerializeField]
    private Sprite _Right;
    [SerializeField]
    private Sprite _Up;
    [SerializeField]
    private Sprite _Down;

    [SerializeField]
    private Sprite[] _Explosion;
    [SerializeField]
    private float _TimePerExplosionFrame;
    int frame = 0;
    float frameDuration = 0.0f;

    private SmoothMovement _Contoller;
    float ghostingTime = 0.0f;

    private void Awake(){
        _Contoller = GetComponent<SmoothMovement>();
    }

    private void Update() {
        if (_Contoller.EnemyBaseCache.IsDestroyed) {
            ghostingTime += Time.deltaTime;

            if (ghostingTime > 1.0f) {
                _View.color = Color.white;

                frameDuration += Time.deltaTime;
                while(frameDuration > _TimePerExplosionFrame) {
                    frameDuration -= _TimePerExplosionFrame;
                    frame++;
                    if(frame >= _Explosion.Length) {
                        PlaySound s = new PlaySound();
                        s.Clip = deathClip;
                        this.Signal<PlaySound>(s);
                        DestroyImmediate(this.gameObject);
                        return;
                    } else {
                        _View.sprite = _Explosion[frame];
                    }
                }

            } else {
                
                float sin = Mathf.Sin(ghostingTime * Mathf.PI * 15.0f);   

                _View.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Max(0.0f, Mathf.Sign(sin)));                
            }

            return;
        }

        var dir = _Contoller.Direction;

        if(Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
            _View.sprite = dir.x > 0.0f ? _Right : _Left;


        } else {
            _View.sprite = dir.y > 0.0f ? _Up : _Down;
        }
    }

}
