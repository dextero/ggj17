﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Abstract class for screen
 */
public class ComboMessageController : MonoBehaviour {
    private Image _Image;
    public Image Image {
        get {
            if (_Image == null) {
                _Image = GetComponentInChildren<Image>();
            }
            return _Image;
        }
    }
}
