﻿using DG.Tweening;
using Moonlit.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TunerView : MonoBehaviour, IMessageReceiver
{
    #region Inspector Variables
    [SerializeField] private RectTransform _Arrow;

    [SerializeField] private RectTransform _Speaker0;
    [SerializeField] private RectTransform _Speaker1;
    [SerializeField]
    private Image _Speaker0Image;
    [SerializeField]
    private Image _Speaker1Image;
    [SerializeField] private float _Upscale = 1.0f;
    [SerializeField] private float _UpscaleTime = 0.1f;
    [SerializeField] private AnimationCurve _Curve;

    private Vector3 _Scale;

    [SerializeField]
    private Sprite[] _RadioStations;

    [SerializeField]
    Text _FM;

    [SerializeField]
    private RectTransform _FirstLine;
    [SerializeField]
    private RectTransform _SecLine;
    [SerializeField]
    private RectTransform _RadioStation;
    [SerializeField]
    private Image _RadioStationLogo;
    float lastTime = 0.0f;
    float lastValue;
    bool playerDied = false;

    private GameController.BeatStation _LastBeat = GameController.BeatStation.NONE;
    #endregion 

    #region Unity Messages
    private void OnEnable()
    {
        this.Register();        
        _Scale = _Speaker0.transform.localScale;
    }

    private void OnDisable()
    {
        this.Unregister();
    }

    protected void Update() {
        lastTime += Time.deltaTime;
                
    }
    #endregion 

    #region Messages
    [MessageSlot]
    private void Receive(SetFrequency msg)
    {
        if (playerDied) return;
        float percentage = msg.Value * 0.01f;

        var rect = transform as RectTransform;

        _Arrow.localRotation = Quaternion.Euler(
            _Arrow.localRotation.eulerAngles.x, 
            _Arrow.localRotation.eulerAngles.y,
            Mathf.Lerp(22.0f, -22.0f, percentage));

        float value = 96.0f + 5.0f * (msg.Value / 100.0f);
        int d = (int)Mathf.Floor(value * 100.0f) % 100;
        _FM.text = string.Format("{0}.{2}{1}fm", Mathf.Floor(value), d, d < 10 ? "0" : "");
        
    }

    Sequence s;

    [MessageSlot]
    private void PlayerDied(PlayerDied death) {
        playerDied = true;
    }

    [MessageSlot]
    private void Receive(Beat msg)
    {
        if (playerDied) return;
        if(msg.Type == AudioChannel.BeatType.Bass)
        {
            Shake();
        }

        if (_LastBeat != msg.BColor) {
            
            _Speaker0Image.color = GameController.Instance.GetColorForBeatStation(msg.BColor);
            _Speaker1Image.color = GameController.Instance.GetColorForBeatStation(msg.BColor);

            _LastBeat = msg.BColor;

            if (s != null) {
                s.Kill(true);
                s = null;
            }
            _FirstLine.DOKill(true);
            _SecLine.DOKill(true);
            _RadioStationLogo.DOKill(true);

            if (_LastBeat != GameController.BeatStation.NONE) {

                Text fLineText = _FirstLine.transform.GetComponent<Text>();
                Text sLineText = _SecLine.transform.GetComponent<Text>();
                RectTransform stationLogoPos = _RadioStationLogo.GetComponent<RectTransform>();

                _FirstLine.anchoredPosition = new Vector2(174 - 30, -1.2f);
                _SecLine.anchoredPosition = new Vector2(174 - 30, -1.2f);
                stationLogoPos.anchoredPosition = new Vector2(266.5f - 30.0f, -12.6f);

                _RadioStationLogo.sprite = _RadioStations[(int)_LastBeat - 1];

                s = 
                DOTween
                    .Sequence();

                    s.Insert(0, _FirstLine
                        .DOAnchorPosX(174, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.02f, 0.73f, 0.18f, 0.95f)))
                    .Insert(0, fLineText.DOFade(1.0f, 0.5f))
                    .Insert(0.25f, _SecLine
                        .DOAnchorPosX(174, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.02f, 0.73f, 0.18f, 0.95f)))
                    .Insert(0.25f, sLineText.DOFade(1.0f, 0.5f))
                    .Insert(0.5f, stationLogoPos
                        .DOAnchorPosX(266.5f, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.02f, 0.73f, 0.18f, 0.95f)))
                    .Insert(0.5f, _RadioStationLogo.DOFade(1.0f, 1.0f))

                    .Insert(0 + 1.5f, _FirstLine
                        .DOAnchorPosX(174 + 50, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.46f, 0.01f, 0.93f, 0.16f)))
                    .Insert(0 + 2f, fLineText.DOFade(0.0f, 0.5f))
                    .Insert(0.25f + 1.5f, _SecLine
                        .DOAnchorPosX(174 + 50, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.46f, 0.01f, 0.93f, 0.16f)))
                    .Insert(0.25f + 2f, sLineText.DOFade(0.0f, 0.5f))
                    .Insert(0.5f + 1.5f, stationLogoPos
                        .DOAnchorPosX(266.5f + 50, 1.0f)
                        .SetEase(AnimationUtils.GetBeizerEasing(0.46f, 0.01f, 0.93f, 0.16f)))
                    .Insert(0.5f + 2f, _RadioStationLogo.DOFade(0.0f, 0.5f));

                    s.AppendCallback(() => {
                        s = null;
                    });
            }
        }
    }

    

    private void Shake()
    {

        _Speaker0.DOKill(true);
        _Speaker0.localScale = new Vector3(0.8f, 0.8f, 0.8f);

        _Speaker0
            .DOScale(1.0f, 0.3f)
            .SetEase(Ease.OutBounce);

        _Speaker1.DOKill(true);
        _Speaker1.localScale = new Vector3(0.8f, 0.8f, 0.8f);

        _Speaker1
            .DOScale(1.0f, 0.3f)
            .SetEase(Ease.OutBounce);

        /*float time = 0.0f;
        while(time < _UpscaleTime)
        {
            time += Time.deltaTime;
            var value = _Curve.Evaluate(time/_UpscaleTime);
            _Speaker0.transform.localScale = _Scale * (1.0f + value * _Upscale);
            _Speaker1.transform.localScale = _Scale * (1.0f + value * _Upscale);
            yield return null;
        }*/

        //_Speaker0.DOKill();
        //_Speaker0.DOScale(1.5f, 0.2f);
    }
    #endregion 


}
