﻿using DG.Tweening;
using Moonlit.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public class Shake
{
    public float Amplitude;
    public float Duration;
}

public class Shakers : MonoBehaviour, IMessageReceiver, IMessageSender {

    private float _Enabled = 0.0f;
    private float _Amplitude = 0.05f;

    public void OnEnable()
    {
        this.Register();
        _Enabled = 10.0f;
    }
    public void OnDisable()
    {
        this.Unregister();
    }
    public void Update()
    {
        if(_Enabled >= 0.0f)
        {
            _Enabled -= Time.deltaTime;
        }
    }

    [MessageSlot]
    private void Receive(Shake msg)
    {
        _Enabled = msg.Duration;
        _Amplitude = msg.Amplitude;
    }
    
    [MessageSlot(DispatchMethod.Subscriber, DispatchPriority.High)]
    private void Receive(PlayerPositionChanged msg) {
        if(_Enabled <= 0.0f) {
            return;
        }
        msg.position += _Amplitude * new Vector3( Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
    }
}
