﻿using Moonlit.Messages;
using System;
using System.Collections.Generic;
using UnityEngine;


#region Audio Messages
[Serializable]
public class SetFrequency { public float Value; }
[Serializable]
public class GetFrequency { public float Value; }
[Serializable]
public class Beat { 
    public AudioChannel.BeatType Type;
    public GameController.BeatStation BColor;

    private static Beat _Instance;

    public static Beat Instance(AudioChannel.BeatType type, GameController.BeatStation bcolor) {
        if (_Instance == null)
            _Instance = new Beat();

        _Instance.Type = type;
        _Instance.BColor = bcolor;

        return _Instance;
    }
};
[Serializable]
public class PlaySound { public AudioClip Clip; }
#endregion 

public class AudioSystem : MonoBehaviour, IMessageReceiver, IMessageSender
{
    #region Inspector Variables
    [Header("SoundSources")]
    [SerializeField]
    private AudioSource _NoiseSource;
    [SerializeField]
    private AudioSource _Effects;

    [Header("Tuner")]
    [SerializeField]
    private float _TunerFrequency;
    [SerializeField]
    private List<AudioChannel> _Channels;

    [Header("Frequency")]
    [SerializeField]
    private float _MinFrequency = 0.0f;
    [SerializeField]
    private float _MaxFrequency = 100.0f;
    [SerializeField]
    private AnimationCurve _Correlation = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
    [SerializeField]
    private float _CrossfadeDistance = 5.0f;
    [SerializeField] private float _MaxNoise = 0.3f;
    #endregion

    #region Private Variables
    private AudioChannel _CurrentChannel;
    private int _PreviousCounter = -1;
    private float _Time;
    #endregion 

    #region Unity Messages
    private void Start()
    {
        SetOutput();
    }
    private void OnEnable()
    {
        Debug.Log("onenable");
        this.Register();
    }
    private void OnDisable()
    {
        this.Unregister();
    }

    private void LateUpdate()
    {
        _Time = (float)_CurrentChannel.Source.timeSamples / (float)_CurrentChannel.Source.clip.frequency;
        if(_CurrentChannel != null)
        {

            var index = _CurrentChannel.GetBeatTempo(_Time);
            if(index != _PreviousCounter)
            {
                _PreviousCounter = index;

                var beatType = _CurrentChannel.GetBeatAtSecond(_Time);
                //if(beatType != AudioChannel.BeatType.None)
                //{
                this.Signal(Beat.Instance(
                    beatType,
                    GetChannelStation(_CurrentChannel)));
                //}
            }
        }
    }
    #endregion 

    #region Messages
    [MessageSlot]
    private void Receive(SetFrequency msg)
    {
        _TunerFrequency = msg.Value;
        SetOutput();
    }

    [MessageSlot]
    private void Receive(GetFrequency msg)
    {
        msg.Value = _TunerFrequency;
        _NoiseSource.volume = 0.0f;
    }

    [MessageSlot]
    private void Receive(Beat msg)
    {
        
    }

    [MessageSlot]
    private void Receive(PlaySound sound) {
        if (sound.Clip == null) return;
        _Effects.PlayOneShot(sound.Clip);
    }
    #endregion

    private float GetVolume(float frequencyDifference)
    {
        float clipped = Mathf.Clamp01(frequencyDifference/_CrossfadeDistance);
        return 1.0f - _Correlation.Evaluate(clipped);
    }

    #region Private Methods
    private void SetOutput()
    {
        float _FrequencyDifference = float.MaxValue;
        foreach(var channel in _Channels)
        {
            if(_TunerFrequency >= channel.StartFrequency && _TunerFrequency < channel.EndFrequency)
            {
                _FrequencyDifference = 0.0f;
                _CurrentChannel = channel;
                channel.Source.volume = 1.0f;
                continue;
            }

            float leftDifference = Mathf.Abs(channel.StartFrequency - _TunerFrequency);
            float rightDifference = Mathf.Abs(channel.EndFrequency - _TunerFrequency);

            var difference = Mathf.Min(leftDifference, rightDifference);

            channel.Source.volume = GetVolume(difference);

            if(difference < _FrequencyDifference)
            {
                _FrequencyDifference = difference;
                _CurrentChannel = channel;
            }
        }

        _NoiseSource.volume = _MaxNoise * (1.0f - GetVolume(_FrequencyDifference));
    }
    #endregion 

    private GameController.BeatStation GetChannelStation(AudioChannel channel) {
        if (channel == _Channels[0])
            return GameController.BeatStation.BLUE;
        if (channel == _Channels[1])
            return GameController.BeatStation.OTHER_RED;
        if (channel == _Channels[2])
            return GameController.BeatStation.RED;
        if (channel == _Channels[3])
            return GameController.BeatStation.OTHER_BLUE;

        return GameController.BeatStation.NONE;
    }
}
