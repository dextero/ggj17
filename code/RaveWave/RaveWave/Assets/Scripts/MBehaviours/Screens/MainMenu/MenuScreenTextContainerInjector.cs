﻿using UnityEngine;
using Zenject;

public class MenuScreenTextContainerInjector : MonoBehaviour {
    [Inject] private MainMenuDialScroller2D scroller;

    [Inject]
	void Inject() {
		scroller.transform.SetParent(transform, false);
	}
}
