﻿using DG.Tweening;
using Moonlit.Messages;
using UnityEngine;

public class BeatScaler2D : MonoBehaviour, IMessageReceiver {
    [SerializeField] protected float _BeatScale = 1.2f;
    [SerializeField] protected float _BeatDurationSeconds = 0.3f;

    void Start() {
        this.Register();
    }

    void OnDestroy() {
        this.Unregister();
    }

    [MessageSlot]
    void OnBeat(Beat beat) {
        if (beat.Type == AudioChannel.BeatType.None) return;

        DOTween.Sequence()
               .Append(transform.DOScale(new Vector3(_BeatScale, _BeatScale), _BeatDurationSeconds))
               .Append(transform.DOScale(new Vector3(1.0f, 1.0f), _BeatDurationSeconds));
    }
}
