﻿using System.Collections.Generic;
using ModestTree.Util;
using Moonlit.Messages;
using UnityEngine;

public class MainMenuDialScroller2D : MonoBehaviour, IMessageReceiver, IMessageSender {
    private RectTransform _Rect;
    private RectTransform Rect {
        get {
            if (_Rect == null) {
                _Rect = GetComponent<RectTransform>();
            }
            return _Rect;
        }
    }

    private float currFreq = 0.0f;
    List<Tuple<float, int>> MARGINS = new List<Tuple<float, int>>() {
        new Tuple<float, int>(15.0f, 0),
        new Tuple<float, int>(25.0f, -1),
        new Tuple<float, int>(40.5f, 1),
        new Tuple<float, int>(55.5f, -1),
        new Tuple<float, int>(70.5f, 2),
        new Tuple<float, int>(80.5f, -1)
    };
    private Vector3 pos;
    private int prevSelection = -1;
    public int SelectedOption {
        get {
            foreach (var tuple in MARGINS) {
                if (currFreq < tuple.First) {
                    return tuple.Second;
                }
            }
            return -1;
        }
    }

	// Use this for initialization
	void Start () {
        this.Register();
        pos = transform.localPosition;
	}

    void OnDestroy() {
        this.Unregister();
    }
	
	// Update is called once per frame
	void Update () {
	    int currSelection = SelectedOption;

	    if (currSelection != prevSelection) {
	        if (currSelection == -1) {
	            this.Signal(new LEDBacklight.Deactivate());
            } else {
                this.Signal(new LEDBacklight.Activate());
	        }
	    }

        prevSelection = currSelection;
	}

    [MessageSlot]
    void OnSetFrequency(SetFrequency freq) {
        currFreq = freq.Value;

        float newY = Rect.rect.height * currFreq / 100.0f;
        transform.localPosition = new Vector3(pos.x, pos.y +  newY, pos.z);

//        Debug.Log(string.Format("freq = {0}, selection: {1}", currFreq, SelectedOption));
    }
}
