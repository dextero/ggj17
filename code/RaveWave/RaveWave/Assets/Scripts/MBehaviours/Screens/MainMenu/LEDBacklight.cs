﻿using System;
using Moonlit.Messages;
using UnityEngine;
using UnityEngine.UI;

public class LEDBacklight : MonoBehaviour, IMessageReceiver {
    [SerializeField] private Color _InactiveColor;
    [SerializeField] private Color _ActiveColor;

    private Image _Image;
    private Image Image {
        get {
            if (_Image == null) {
                _Image = GetComponent<Image>();
            }
            return _Image;
        }
    }

    [Serializable]
    public class Activate {};
    [Serializable]
    public class Deactivate {};

	void Start () {
        this.Register();
	    OnDeactivate(null);
	}

    void OnDestroy() {
        this.Unregister();
    }

    [MessageSlot]
    void OnActivate(Activate activate) {
        Image.color = _ActiveColor;
    }

    [MessageSlot]
    void OnDeactivate(Deactivate deactivate) {
        Image.color = _InactiveColor;
    }
}
