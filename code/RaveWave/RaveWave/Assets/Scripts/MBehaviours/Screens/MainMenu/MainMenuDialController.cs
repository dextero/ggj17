﻿using Moonlit.Messages;
using UnityEngine;

public class MainMenuDialController : MonoBehaviour, IMessageReceiver {
    void Start() {
        this.Register();
    }

    void OnDestroy() {
        this.Unregister();
    }

    [MessageSlot]
    void OnSetFrequency(SetFrequency freq) {
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, -freq.Value / 100.0f * 360.0f);
    }
}
