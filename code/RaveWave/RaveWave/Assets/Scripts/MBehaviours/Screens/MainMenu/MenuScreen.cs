﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;
using Zenject;

public class MenuScreen : AScreen {
    [Inject] protected IInputHandler input;
    [Inject] protected MainMenuDialScroller2D menuDialScroller;

	void Start () {
	}
	
	void FixedUpdate () {
	    if ((input.PrevState.Buttons.A == ButtonState.Pressed && input.State.Buttons.A == ButtonState.Released)
	        || (input.PrevState.Buttons.RightShoulder == ButtonState.Pressed && input.State.Buttons.RightShoulder == ButtonState.Released)
	        || (input.PrevState.Buttons.RightStick == ButtonState.Pressed && input.State.Buttons.RightStick == ButtonState.Released)) {
            switch (menuDialScroller.SelectedOption) {
            case 0: // new game
                GameController.Instance.ShowScreen(GameController.ScreenTypes.GameScreen);
                break;
            case 1: // highscores
                GameController.Instance.ShowScreen(GameController.ScreenTypes.HighScoreMenu);
                break;
            case 2: // credits: TODO
                break;
            case 3: // exit: TODO
                Application.Quit();
                break;
            default: // TODO: "denied" sound
                break;
            }
	    }
	}

    [ContextMenu("Show HI Scores")]
    public void GoToHiScores() {
        GameController.Instance.ShowScreen(GameController.ScreenTypes.HighScoreMenu);
    }

    [ContextMenu("Show Game")]
    public void ShowGame() {
        GameController.Instance.ShowScreen(GameController.ScreenTypes.GameScreen);
    }

    public class Factory : Factory<MenuScreen> { }
}
