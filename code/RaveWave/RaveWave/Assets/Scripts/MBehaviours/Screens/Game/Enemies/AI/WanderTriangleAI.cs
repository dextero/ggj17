﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(EnemyBase))]
public class WanderTriangleAI : EnemyBase.EnemyAI {
    
    Vector2 _MovementDirection;            
    float _DashDuration = 10.0f;

    EaseFunction _SlowWalkEase = AnimationUtils.GetBeizerEasing(0.53f, 0.17f, 0.59f, 0.97f);
    Vector2 _NextMovementDirection;

    float lastFire;

    float lastAngle = 0.0f;

    bool performMove = false;

    float direction = 1.0f;

    float lerpValue = 0.2f;

    [Inject]
    protected void Inject() {
        ChangeDir();

        EnemyBaseCache.EnemyMovementComponent.MaxMovementVelocity = 8.0f;
                

        performMove = true;
        lastAngle = UnityEngine.Random.Range(0.0f, 360.0f);
        
        _MovementDirection = (new Vector2(50, 23) - EnemyBaseCache.Position)
           .Rotate(-(90 / 2) + UnityEngine.Random.Range(0.0f, 90));
        _NextMovementDirection = _MovementDirection.Rotate(
            direction * 120 - 10 + UnityEngine.Random.Range(0, 20));

        direction = UnityEngine.Random.Range(0, 2) == 0 ? 1.0f : -1.0f;

        
    }

    protected void ChangeDir() {
        _MovementDirection = _NextMovementDirection;
        _NextMovementDirection = _MovementDirection.Rotate(
            direction * 120 - 10 + UnityEngine.Random.Range(0, 20));
    }

    protected void Update() {
        if (EnemyBaseCache.EnemyMovementComponent == null) return;        
        
    }

    protected void OnEnable() {        
        StartCoroutine(MoveIntoNewLocation());  
    }

    protected void OnDisable() {
        StopAllCoroutines();
    }

    IEnumerator MoveIntoNewLocation() {
        while (this.gameObject.activeSelf) {

            lerpValue = UnityEngine.Random.Range(0.15f, 0.3f);

            ChangeDir();

            float velocity =
                EnemyBaseCache.EnemyMovementComponent.MaxMovementVelocity *
                UnityEngine.Random.Range(0.8f, 1.0f);

            float dashTimeOrig = _DashDuration + UnityEngine.Random.Range(-1.0f, 1.5f);
            float dashTime = dashTimeOrig;

            do {
                float percent = 1.0f - _SlowWalkEase((dashTimeOrig - dashTime), dashTimeOrig, 0.0f, 0.0f);
                Vector2 mDir = _MovementDirection;

                if (percent > (1.0f - lerpValue)) {
                    float lerpPercent = (percent - (1.0f - lerpValue)) / lerpValue;
                    mDir = Vector2.Lerp(
                        _MovementDirection,
                        _NextMovementDirection,
                        lerpPercent);
                }

                bool succeded =
                    EnemyBaseCache
                        .EnemyMovementComponent
                        .MoveInDirection(mDir, velocity * (percent * 0.5f + 0.5f));

                dashTime -= Time.deltaTime;
                yield return new WaitForEndOfFrame();

                if (!succeded)
                    dashTime = 0.0f;

            } while (dashTime > 0.0f);            

            if (UnityEngine.Random.Range(0, 3) == 0 && EnemyBaseCache.EnemyAttackComponent != null) {
                if (EnemyBaseCache.EnemyMovementComponent != null &&
                    !EnemyBaseCache.EnemyMovementComponent.StopMovement) {                    

                    EnemyBaseCache.EnemyAttackComponent.PerformAttack();

                    if (!(EnemyBaseCache.EnemyAttackComponent is SineWaveShooter)) {
                        yield return new WaitForSeconds(1.0f);
                    }
                }
            }

            if (UnityEngine.Random.Range(0, 6) == 0) {
                direction *= -1.0f;
            }

        }        
    }
}

