﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(EnemyBase))]
public class RoundShooter : EnemyBase.EnemyAttack, IMessageSender {
    #region Config
    [SerializeField]
    protected float _BulletVelocity = 50;
    [SerializeField]
    protected float _ShootDelta = 0.15f;
    [SerializeField]
    protected int _BulletCount = 10;
    [SerializeField]
    protected float _AngleDelta = 10.0f;
    [SerializeField]
    protected AudioClip roarSound;
    #endregion

    private bool _PerformingAttack = false;

    public override void PerformAttack() {
        if (EnemyBaseCache.IsDestroyed) return;
        StartCoroutine(AttackCoroutine());
    }

    IEnumerator AttackCoroutine() {
        if (EnemyBaseCache.EnemyMovementComponent != null)
            EnemyBaseCache.EnemyMovementComponent.StopMovement = true;

        yield return new WaitForSeconds(0.5f);

        float startAngle = UnityEngine.Random.Range(0, 360);

        _BulletCount = 10 + UnityEngine.Random.Range(0, 10);

        GameController.BeatStation randomColor
            = Utilities.RandomEnum<GameController.BeatStation>();

        PlaySound sound = new PlaySound();
        sound.Clip = roarSound;
        this.Signal<PlaySound>(sound);

        for (int i = 0; i < _BulletCount; i++) {
            yield return new WaitForSeconds(_ShootDelta);

            this.Signal(MessageShootBullet.Instance(
                EnemyBaseCache.Position, 
                startAngle + _AngleDelta * i, 
                _BulletVelocity,
                randomColor));
            this.Signal(MessageShootBullet.Instance(
                EnemyBaseCache.Position,
                startAngle + _AngleDelta * i + 180.0f,
                _BulletVelocity,
                randomColor));            
        }
       
        yield return new WaitForSeconds(1.0f);

        EnemyBaseCache.EnemyMovementComponent.StopMovement = false;
    }
}