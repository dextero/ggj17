﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moonlit.Messages;
using UnityEngine;
using Zenject;

[Serializable]
public class EnemyBase : GameEntity, IMessageSender, IMessageReceiver {

    #region Configuration
    [SerializeField]
    [Range(1, 10)]
    protected int _Damage;
    #endregion    

    #region Cache 
    EnemyAttack _EnemyAttack;
    EnemyAI _EnemyAI;
    EnemyMovement _EnemyMovement;
    RectTransform _RectTransform;
    [SerializeField]
    SpriteRenderer _Renderer;
    [SerializeField]
    Material _BlueMaterial;
    [SerializeField]
    Material _RedMaterial;
    #endregion

    #region Properties
    public bool StopMovement = false;

    protected bool Destroyed = false;

    public bool IsDestroyed {
        get { return Destroyed; }
    }

    private SpriteRenderer Renderer {
        get {            
            return _Renderer;
        }
    }    

    protected override void Awake() {
        base.Awake();

        if (UnityEngine.Random.Range(0, 10) == 0) {
            this.gameObject.AddComponent<DashAI>();

            EnemyBase.EnemyAttack attack = this.gameObject.GetComponent<EnemyBase.EnemyAttack>();
            if (attack != null) {
                DestroyImmediate(attack);
            }
            this.gameObject.AddComponent<SineWaveShooter>();


        } else {            
            this.gameObject.AddComponent<WanderTriangleAI>();
        }

        _BeatType = Utilities
            .RandomEnum<GameController.BeatStation>();   
        
    }

    public void SetLightningColor() {
        Renderer.material = _RedMaterial;
        Renderer.color = Color.black;
    }

    public void SetActColor() {
        Renderer.color = Color.white;

        if (GameController.AreBeatsEqual(_BeatType, GameController.BeatStation.RED)) {
            Renderer.material = _RedMaterial;            
        } else {
            Renderer.material = _BlueMaterial;
        }        
    }

    public EnemyAttack EnemyAttackComponent {
        get { 
            if(_EnemyAttack == null)
                _EnemyAttack = GetComponent<EnemyAttack>();
            return _EnemyAttack;
        }
    }

    public EnemyAI EnemyAIComponent {
        get {
            if (_EnemyAI == null)
                _EnemyAI = GetComponent<EnemyAI>();
            return _EnemyAI;
        }
    }

    public EnemyMovement EnemyMovementComponent {
        get {
            if (_EnemyMovement == null)
                _EnemyMovement = GetComponent<EnemyMovement>();
            return _EnemyMovement;
        }
    }
    #endregion

    private GameController.BeatStation _BeatType;

    public GameController.BeatStation BeatType {
        get { return _BeatType; }
    }

    protected void Update() {        
    }

    public void KillByWave(BeatWaveController beat) {
        if (Destroyed) return;

        if (GameController.AreBeatsEqual(beat.BeatType, BeatType)) {
            //kill this
            if (this.gameObject != null && !IsDestroyed) {
                DebugKill();
            }
            this.Signal(new ComboCounter.Hit());            

        } else {
            //TODO: set color
            // lol @ "not affected"
            //this.Signal(new ComboCounter.Hit());
        }        
    }

    [ContextMenu("Kill")]
    public void DebugKill() {
        Destroyed = true;
        this.Signal(new ComboCounter.EnemyKilled());
        StopAllCoroutines();   
    }

    [MessageSlot]
    private void Receive(PlayerDied msg) {
        if (GetComponent<EnemyAI>() != null) {
            GetComponent<EnemyAI>().StopAllCoroutines();
            GetComponent<EnemyAI>().enabled = false;
        }
        if (GetComponent<EnemyMovement>() != null) {
            GetComponent<EnemyMovement>().StopAllCoroutines();
            GetComponent<EnemyMovement>().enabled = false;
        }
    }

    protected void OnEnable() {
        this.Register();
    }    

    public void CollidedWithPlayer() {
        Debug.Log("Collided with player");
    }

    protected override void OnDisable() {
        this.Unregister();
        Destroyed = true;
        base.OnDisable();
        StopAllCoroutines();
    }

    #region Abstract Classes
    public abstract class EnemyComponent : MonoBehaviour {

        public EnemyBase EnemyBaseCache {
            get {
                if (_EnemyBaseCache == null)
                    _EnemyBaseCache = this.GetComponent<EnemyBase>();
                return _EnemyBaseCache; 
            }
        }

        protected EnemyBase _EnemyBaseCache;        
    }

    [Serializable]
    public abstract class EnemyAttack : EnemyComponent {
        public abstract void PerformAttack();
    }

    [Serializable]
    public abstract class EnemyAI : EnemyComponent {
        protected void OnDisable() {
            this.StopAllCoroutines();
        }
    }

    [Serializable]
    public abstract class EnemyMovement : EnemyComponent {
        [SerializeField]
        protected float _MaxMovementVelocity;

        public bool StopMovement = false;

        public Vector2 Direction;

        public float MaxMovementVelocity {
            get { return _MaxMovementVelocity; }
            set { _MaxMovementVelocity = value; }
        }


        public abstract bool MoveInDirection(Vector2 direction, float velocity);
    }
    #endregion

    #region Factories
    public class EnemyFlower : Factory<EnemyBase> { }
    public class EnemySine : Factory<EnemyBase> { }
    public class EnemyRound : Factory<EnemyBase> { }
    #endregion
}