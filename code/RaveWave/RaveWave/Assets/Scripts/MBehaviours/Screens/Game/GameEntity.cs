﻿using DG.Tweening;
using Moonlit.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

[Serializable]
public class MessageEntityBorn { 
    public GameEntity Entity;    

    public MessageEntityBorn() { }

    private static MessageEntityBorn _Instance;

    public static MessageEntityBorn Instance(GameEntity e) {
        if (_Instance == null)
            _Instance = new MessageEntityBorn();

        _Instance.Entity = e;

        return _Instance;
    }
}

[Serializable]
public class MessageEntityDied { 
    public GameEntity Entity; 

    public MessageEntityDied() { }

    private static MessageEntityDied _Instance;

    public static MessageEntityDied Instance(GameEntity e) {
        if (_Instance == null)
            _Instance = new MessageEntityDied();

        _Instance.Entity = e;

        return _Instance;
    }
}

[Serializable]
public class MessageEntityMoved { 
    public GameEntity Entity; 

    public MessageEntityMoved() { }

    private static MessageEntityMoved _Instance;

    public static MessageEntityMoved Instance(GameEntity e) {
        if (_Instance == null)
            _Instance = new MessageEntityMoved();

        _Instance.Entity = e;

        return _Instance;
    }
}

[Serializable]
public class GameEntity : MonoBehaviour, IMessageSender {    

    #region Config
    [SerializeField]
    protected float _HitBoxRadius = 1.0f;
    [SerializeField]
    protected bool _Collidable = true;
    #endregion

    #region Values
    protected Vector2 _Position;    
    protected InPlaceArray<Vector2> _RegisteredAtGridPositions = new InPlaceArray<Vector2>(30);
    #endregion

    #region Properties
    public virtual float HitBoxRadius {
        set {
            _HitBoxRadius = value;
            //TODO: update content
        }
        get { return _HitBoxRadius; }
    }

    public bool Collidable {
        get { return _Collidable; }
    }

    //assumes new position is valid
    public Vector2 Position {
        get { return _Position; }
        set {             
            _Position = value;

            if (GameController.Instance.Game == null) return;

            Vector2 positionScale = new Vector2(
                _Position.x / GameController.Instance.GameplayMapSize.x,
                _Position.y / GameController.Instance.GameplayMapSize.y);
            Vector2 targetSizeDiff = new Vector2(
                GameController.Instance.Game.TopRight.position.x - GameController.Instance.Game.LowerLeft.position.x,
                GameController.Instance.Game.TopRight.position.y - GameController.Instance.Game.LowerLeft.position.y
                );
            Vector2 targetPosition = new Vector2(
                GameController.Instance.Game.LowerLeft.position.x + targetSizeDiff.x * positionScale.x,
                GameController.Instance.Game.LowerLeft.position.y + targetSizeDiff.y * positionScale.y
                );            

            transform.position = targetPosition;
            Utilities.SetSpriteDepth(transform);

            this.Signal<MessageEntityMoved>(MessageEntityMoved.Instance(this));
        }
    }

    public InPlaceArray<Vector2> RegisteredAtGridPositions {
        get { return _RegisteredAtGridPositions; }

    }
    #endregion

    protected virtual void Awake() {}

    [Inject]
    protected void Inject() {
        _RegisteredAtGridPositions.Clear();   
    }

    protected virtual void OnEnable() {
        this.Signal<MessageEntityBorn>(MessageEntityBorn.Instance(this));
    }

    protected virtual void OnDisable() {
        this.Signal<MessageEntityDied>(MessageEntityDied.Instance(this));
    }
}
