﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[Serializable]
public class MessageShootBullet { 
    public Vector2 StartPosition; 
    public float Direction; 
    public float Velocity;
    public float SineWaveDelta;
    public GameController.BeatStation ColorType;

    public MessageShootBullet() {}

    private static MessageShootBullet _Instance;

    public static MessageShootBullet Instance(
        Vector2 startPosition,
            float direction,
            float velocity,
            GameController.BeatStation cType,
            float sineWaveDelta = 0.0f) {

            if (_Instance == null)
                _Instance = new MessageShootBullet();

            _Instance.ColorType = cType;
            _Instance.StartPosition = startPosition;
            _Instance.Direction = direction;
            _Instance.Velocity = velocity;
            _Instance.SineWaveDelta = sineWaveDelta;

            return _Instance;
    }
}

public class GameScreen : AScreen, IMessageReceiver {    

    #region Construction Factories
    [Inject]
    EnemyBase.EnemySine _Enemy1Factory;

    [Inject]
    EnemyBase.EnemyRound _Enemy2Factory;

    [Inject]
    EnemyBase.EnemyFlower _Enemy3Factory;

    [Inject]
    Lighting.Factory _LightningFactory;
    #endregion

    public Transform _Parallax;
    private float FirstTimeSpawnDelay = 1.0f;

    #region Configuration
    public Transform LowerLeft;
    public Transform TopRight;
    #endregion

    #region Collision Detection
    InPlaceArray<GameEntity>[,] _CollisionData;    
    #endregion

    List<Bullet> _Bullets;

    [Inject]
    private PlayerController.Factory _PlayerFactory;

    [Inject] 
    private TunerView tunerView;

    [Inject]
    private HealthView healthView;

    [Inject]
    private Bullet.BasicBullet _BulletFactory;

    [Inject] private ComboCounter _ComboCounter;

    public PlayerController _Player;

    private InPlaceArray<GameEntity> _Entities = new InPlaceArray<GameEntity>(50);

    private InPlaceArray<GameEntity> _CircledEntities = new InPlaceArray<GameEntity>(10);

    private int defeatedEnemies = 0;

    private int currentEnemies = 0;

    [Inject]
    void Inject() {
        this.Register();

        _Bullets = new List<Bullet>();

        CleanCollisionData();

        _Player = _PlayerFactory.Create();
        _Player.transform.SetParent(this.transform, false);
        _Player.Position = _Player.InitialPosition;        
    }
      private void CleanCollisionData() {
        int mapWidth = (int)GameController.Instance.GameplayMapSize.x;
        int mapHeight = (int)GameController.Instance.GameplayMapSize.y;

        _CollisionData = new InPlaceArray<GameEntity>[mapWidth, mapHeight];
        for (int i = 0; i < mapWidth; i++)
            for (int j = 0; j < mapHeight; j++)
                _CollisionData[i, j] = new InPlaceArray<GameEntity>(10);
    }

    void Start () {
        
    }

    void OnDestroy() {
        this.Unregister();
    }
    
    // Update is called once per frame
    void Update () {
        // spawning enemies

        int enemiesAtOnce = 7;
        if (defeatedEnemies < 1) {
            enemiesAtOnce = 3;
        } else if (defeatedEnemies < 3) {
            enemiesAtOnce = 7;
        } else if (defeatedEnemies < 6) {
            enemiesAtOnce = 10;
        } else if (defeatedEnemies < 9) {
            enemiesAtOnce = 15;
        } else if (defeatedEnemies < 12) {
            enemiesAtOnce = 20;
        } else if (defeatedEnemies < 15) {
            enemiesAtOnce = 25;
        } else {
            enemiesAtOnce = 30;
        }

        while (currentEnemies < enemiesAtOnce) {
            StartCoroutine(PerformSpawnWithDelay(FirstTimeSpawnDelay + UnityEngine.Random.Range(0.0f, 1.5f)));
        }
        FirstTimeSpawnDelay = 0.0f;
    }

    protected IEnumerator PerformSpawnWithDelay(float delay) {
        currentEnemies++;
        yield return new WaitForSeconds(delay);
        PerformSpawn();
    }

    protected void PerformSpawn() {
        for (int i = 0; i < 5; i++) {
            Vector2 positionCandidate = 
                new Vector2(
                    UnityEngine.Random.Range(0 + 4, 100 - 4), 
                    UnityEngine.Random.Range(0 + 4, 46 - 4));            

            if ((_Player.Position - positionCandidate).magnitude > 10) {
                SpawnEnemy(positionCandidate);
                return;
            }
        }        
    }

    protected void SpawnEnemy(Vector2 position) {
        if (EntitiesCollidingWithCircle(position, 2).Count != 0)
            return;               

        var lightning = _LightningFactory.Create();
        lightning.transform.SetParent(this.transform, false);

        int randomEnemyFactory = UnityEngine.Random.Range(0, 3);
        EnemyBase enemy = null;

        switch (randomEnemyFactory) {
            case 0:
                enemy = _Enemy2Factory.Create();
                break;
            case 1:
                enemy = _Enemy3Factory.Create();
                break;
            default:
                enemy = _Enemy1Factory.Create();
                break;
        }

        enemy.transform.SetParent(this.transform, false);
        enemy.Position = position;

        EnemyBase.EnemyAI AI = enemy.GetComponent<EnemyBase.EnemyAI>();
        AI.enabled = false;
        enemy.SetLightningColor();

        lightning.Calback = () => {            
            enemy.SetActColor();
            AI.enabled = true;    
        };

        lightning.Position = position;

    }

    protected void RemoveCollisionMarkers(GameEntity entity) {
        if (!entity.Collidable) return;

        // remove old position markers
        for (int i = 0; i < entity.RegisteredAtGridPositions.Count; i++) {
            Vector2 marker = entity.RegisteredAtGridPositions.Get(i);
            _CollisionData[(int)marker.x, (int)marker.y].Remove(entity);
        }            
                
        entity.RegisteredAtGridPositions.Clear();
    }

    protected void UpdateEntityPosition(GameEntity entity) {
        if (!entity.Collidable) return;

        RemoveCollisionMarkers(entity);

        // put new position markers        
        entity
            .RegisteredAtGridPositions
            .AddRange(CollisionUtils.CircleOccupyGrid(entity.Position, entity.HitBoxRadius));

        // fill out new position markers
        for (int i = 0; i < entity.RegisteredAtGridPositions.Count; i++) {
            Vector2 marker = entity.RegisteredAtGridPositions.Get(i);
            _CollisionData[(int)marker.x, (int)marker.y].Add(entity);
        }            
    }

    public InPlaceArray<GameEntity> EntitiesAtPoint(Vector2 point) {
        return EntitiesCollidingWithCircle(point, 0.0f);
    }

    public InPlaceArray<GameEntity> EntitiesAtPoints(InPlaceArray<Vector2> points) {
        _Entities.Clear();

        for (int i = 0; i < points.Count; i++)
            EntitiesCollidingWithCircle(points.Get(i), 0.0f, false);

        return _Entities;
    }

    /*
     * Angle segment: 
     *  x: angle Start 
     *  y: angle delta
     */
    public InPlaceArray<GameEntity> EntitiesAtCircleBorder(Vector2 circlePos, float radius, float borderWidth, InPlaceArray<Vector2> angleSegments) {                

        InPlaceArray<GameEntity> entities = EntitiesCollidingWithCircle(circlePos, radius);
        _CircledEntities.Clear();

        for (int i = 0; i < entities.Count; i++) {
            GameEntity entity = entities.Get(i);
            Vector2 distance = (entity.Position - circlePos);
            float angle360 = distance.Angle360(Vector2.right);

            if (distance.magnitude < radius - borderWidth) {                
                continue;
            }

            if (_CircledEntities.Contains(entity))
                continue;

            for (int j = 0; j < angleSegments.Count; j++) {
                Vector2 angleRange = angleSegments.Get(j);
                //skip empty ranges
                if (angleRange.y == 0) continue;

                if (angleRange.x <= angle360 &&
                    angleRange.x + angleRange.y >= angle360) {

                    _CircledEntities.Add(entity);
                    break;
                }
            }
        }

        return _CircledEntities;
    }

    // does not skip self
    public InPlaceArray<GameEntity> EntitiesCollidingWithCircle(Vector2 position, float radius, bool clearArray = true) {
        if (clearArray)
            _Entities.Clear();
        InPlaceArray<Vector2> tilesToTest = CollisionUtils.CircleOccupyGrid(position, radius);

        for (int i = 0; i < tilesToTest.Count; i++) {
            Vector2 tile = tilesToTest.Get(i);
            InPlaceArray<GameEntity> entities = _CollisionData[(int)tile.x, (int)tile.y];

            for (int j = 0; j < entities.Count; j++) {
                GameEntity entity = entities.Get(j);

                if (CollisionUtils.DoesCircleIntersect(entity.Position, entity.HitBoxRadius, position, radius))
                    if (!_Entities.Contains(entity))
                        _Entities.Add(entity);
            }
        }        

        return _Entities;
    }

    #region Messages
    [MessageSlot]
    private void Receive(MessageEntityBorn msg) {
        UpdateEntityPosition(msg.Entity);
    }

    [MessageSlot]
    private void Receive(MessageEntityMoved msg) {
        UpdateEntityPosition(msg.Entity);
    }

    [MessageSlot]
    private void Receive(MessageEntityDied msg) {        
        RemoveCollisionMarkers(msg.Entity);
    }

    [MessageSlot]
    private void Receive(MessageShootBullet msg) {
        Bullet bullet = null;

        foreach(Bullet b in _Bullets)
            if (!b.gameObject.activeSelf) {
                bullet = b;
                break;
            }

        if (bullet == null) {
            bullet = _BulletFactory.Create();
            _Bullets.Add(bullet);
        }

        bullet.gameObject.SetActive(true);
        bullet.Initialise(msg);
    }

    [MessageSlot]
    private void Receive(ComboCounter.EnemyKilled msg) {
        defeatedEnemies++;
        currentEnemies--;
        
    }

    [MessageSlot]
    private void Receive(PlayerPositionChanged msg) {
        float pPercent = (1.0f - (Mathf.Clamp(msg.position.y, 1.0f, 1.2f) - 1.0f) / 0.2f);        
        _Parallax.localPosition = 
            new Vector3(
                _Parallax.localPosition.x,
                pPercent * (-2.0f) + 1.0f,
                _Parallax.localPosition.z);        
    }

    [MessageSlot]
    private void Receive(ComboCounter.Hit hit) {        
    }
    #endregion

    #region Tests
    [ContextMenu("Test Collision Handling")]
    public void PerformCollisionTest (){
        Debug.Log("*** Circle Intersection Test ***");        

        Debug.Assert(CollisionUtils.CircleRectIntersection(new Vector2(0, 0), 0.5f, new Rect(0, 0, 1, 1)));
        Debug.Assert(CollisionUtils.CircleRectIntersection(new Vector2(1, 1), 0.5f, new Rect(0, 0, 1, 1)));
        Debug.Assert(!CollisionUtils.CircleRectIntersection(new Vector2(2, 2), 0.5f, new Rect(0, 0, 1, 1)));
        Debug.Assert(CollisionUtils.CircleRectIntersection(new Vector2(1.5f, 1.5f), 0.5f, new Rect(1, 1, 1, 1)));
        Debug.Assert(CollisionUtils.CircleRectIntersection(new Vector2(1.5f, 0.55f), 0.5f, new Rect(1, 1, 1, 1)));
        Debug.Assert(CollisionUtils.CircleRectIntersection(new Vector2(0.55f, 1.5f), 0.5f, new Rect(1, 1, 1, 1)));
    
    }
    #endregion

    public class Factory : Factory<GameScreen> { }
}
