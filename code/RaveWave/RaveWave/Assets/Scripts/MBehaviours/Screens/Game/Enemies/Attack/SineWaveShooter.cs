﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(EnemyBase))]
public class SineWaveShooter : EnemyBase.EnemyAttack, IMessageSender {
    #region Config
    
    protected float _BulletVelocity = 8;    
    protected float _ShootDelta = 0.4f;    
    protected int _BulletCount = 8;    
    protected float _AngleDelta = 15.0f;
    #endregion

    [SerializeField]
    protected AudioClip roarSound;

    private bool _PerformingAttack = false;

    public override void PerformAttack() {
        if (EnemyBaseCache.IsDestroyed) return;
        StartCoroutine(AttackCoroutine());
    }

    IEnumerator AttackCoroutine() {
        if (EnemyBaseCache.EnemyMovementComponent != null)
            EnemyBaseCache.EnemyMovementComponent.StopMovement = true;

        yield return new WaitForSeconds(0.5f);

        float startAngle = UnityEngine.Random.Range(0, 360);
        if (GameController.Instance.Game != null) {
            Vector2 delta = (GameController.Instance.Game._Player.Position - EnemyBaseCache.Position);
            delta.Normalize();

            startAngle = Mathf.Rad2Deg * (Mathf.Atan2(delta.y, delta.x));// Vector2.Angle(Vector2.right, delta);
        }

        PlaySound sound = new PlaySound();
        sound.Clip = roarSound;
        this.Signal<PlaySound>(sound);

        float sineWave = 1;
        GameController.BeatStation secRandomColor
            = Utilities.RandomEnum<GameController.BeatStation>();

        for (int i = 0; i < _BulletCount; i++) {
            yield return new WaitForSeconds(_ShootDelta);

            this.Signal(MessageShootBullet.Instance(
                EnemyBaseCache.Position,
                startAngle,
                _BulletVelocity,
                secRandomColor,
                sineWave));     
       
            sineWave *= -1.0f;
        }

        yield return new WaitForSeconds(1.0f);

        EnemyBaseCache.EnemyMovementComponent.StopMovement = false;
    }
}