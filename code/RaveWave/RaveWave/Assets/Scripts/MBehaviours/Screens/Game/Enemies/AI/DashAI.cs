﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(EnemyBase))]
public class DashAI : EnemyBase.EnemyAI {

    [SerializeField]
    Vector2 _MovementDirection;    
    float _DashDuration = 3f;    
    float _DashAngle = 60f;

    EaseFunction dashEase;

    float lastFire;

    float lastAngle = 0.0f;

    bool performMove = false;



    [Inject]
    protected void Inject() {
        ChangeDir();

        EnemyBaseCache.EnemyMovementComponent.MaxMovementVelocity = 50.0f;
        dashEase = AnimationUtils.GetBeizerEasing(0.09f, 0.4f, 0.13f, 1.0f);

        performMove = true;
        lastAngle = UnityEngine.Random.Range(0.0f, 360.0f);
    }

    protected void ChangeDir() {

        if (GameController.Instance.Game == null)
            return;

        _MovementDirection = (GameController.Instance.Game._Player.Position - EnemyBaseCache.Position)
            .Rotate(-(_DashAngle / 2) + UnityEngine.Random.Range(0.0f, _DashAngle));

        /*float newAngle = UnityEngine.Random.Range(0.0f, 360.0f);

        while (Vector2.Angle(Vector2.right.Rotate(newAngle), _MovementDirection) < 30.0f) {
            newAngle = UnityEngine.Random.Range(0.0f, 360.0f);
        }

        _MovementDirection = Vector2.right;
        _MovementDirection = _MovementDirection.Rotate(newAngle);

        lastAngle = newAngle;*/
    }

    protected void Update() {
        if (EnemyBaseCache.IsDestroyed) return;
        if (EnemyBaseCache.EnemyMovementComponent == null) return;
        if (EnemyBaseCache.EnemyMovementComponent.StopMovement)
            return;

        if (performMove) {
            performMove = false;
            StartCoroutine(DashIntoNewLocation());            
        }
    }

    IEnumerator DashIntoNewLocation() {        
        int times = UnityEngine.Random.Range(1, 2);

        while (times > 0) {
            ChangeDir();
            
            float velocity = 
                EnemyBaseCache.EnemyMovementComponent.MaxMovementVelocity *
                UnityEngine.Random.Range(0.8f, 1.0f);            

            float dashTimeOrig = _DashDuration + UnityEngine.Random.Range(0.0f, 0.5f);
            float dashTime = dashTimeOrig;

            do {

                float percent = 1.0f - dashEase((dashTimeOrig - dashTime), dashTimeOrig, 0.0f, 0.0f);

                bool succeded =
                    EnemyBaseCache
                        .EnemyMovementComponent
                        .MoveInDirection(
                            _MovementDirection,
                            velocity * percent);

                dashTime -= Time.deltaTime;
                yield return new WaitForEndOfFrame();

                if (!succeded)
                    dashTime = 0.0f;

            } while (dashTime > 0.0f);

            times--;

            if(times > 0)
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.0f, 0.1f));
        }

        if (EnemyBaseCache.EnemyAttackComponent != null) {                        
            EnemyBaseCache.EnemyAttackComponent.PerformAttack();
        }

        performMove = true;
    }
}

