﻿using Moonlit.Messages;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;
using XInputDotNetPure;
using Zenject;
using DG.Tweening;
using System;

[Serializable]
public class PlayerDamaged { public float Current; public float Max; };

[Serializable]
public class ScoreGained {
    public int Value;

    public ScoreGained(int value) {
        Value = value;
    }
}

[Serializable]
public class GameOver {
    public int FinalScore;

    public GameOver(int finalScore) {
        FinalScore = finalScore;
    }
}

[Serializable]
public class PlayerPositionChanged { 
    public Vector3 position;

    static private PlayerPositionChanged _Instance;

    public static PlayerPositionChanged GetInstance(Vector3 position) {
        if(_Instance == null)
            _Instance = new PlayerPositionChanged();
        _Instance.position= position;

        return _Instance;
    }
}

[Serializable]
public class PlayerDied {
    public PlayerDied() { }
}

public class PlayerController : GameEntity, IMessageReceiver, IMessageSender {
    #region Configuration
    [SerializeField]
    protected float _PlayerSpeed = 25.0f;
    [SerializeField]
    public Vector2 InitialPosition = new Vector2(50, 50);
    [SerializeField]
    protected float _GhostingTime = 1.0f;

    [SerializeField] protected int _InitialHp = 3;

    [SerializeField] protected Color _RegularColor = Color.white;
    [SerializeField] protected Color _GhostingColor = Color.white.CopyWithAlpha(0.3f);
    #endregion    

    public Vector2 Direction
    {
        get
        {
            return _Direction;
        }
    }

    [Inject] private Camera camera;

    [SerializeField]
    AudioClip hitSound;

    [Inject] private BeatWaveController.Factory beatWaveFactory;
    private BeatWaveController lastBeatWave;
    private int beatHoldAccumulator = 0;
    private Vector2 _Direction;
    public float _GhostTime;
    private float _MovementStarted = 0.0f;

    private EaseFunction StartMovement;
    private int score = 0;

    private bool playerWasDad = false;

    private int _CurrHp;
    public int CurrHp {
        get { return _CurrHp; }
        set {
            if (value < _CurrHp) {
                int damage = _CurrHp - value;

                if (_CurrHp > 0) {
                    _CurrHp = value;
                    OnDamageReceived(damage);
                }
                // no point in counting overkill
            } else {
                _CurrHp = value;
                // OnHealingReceived?
            }
        }
    }

    [Inject] private IInputHandler input;

    private SpriteRenderer _SpriteRenderer;
    private SpriteRenderer SpriteRenderer {
        get {
            if (_SpriteRenderer == null) {
                _SpriteRenderer = transform.FindChild("View").GetComponent<SpriteRenderer>();
            }
            return _SpriteRenderer;
        }
    }

    [Inject]
    protected void Inject() {
        StartMovement = AnimationUtils.GetBeizerEasing(0.05f, 1.1f, 1f, 1f);
    }

    private void OnEnable() {       
        base.OnEnable();
        this.Register();
    }

    private void OnDisable() {
        base.OnDisable();
        this.Unregister();
    }

    void Start() {        
        CurrHp = _InitialHp;
    }

    void FixedUpdate() {
        /*if (input.PrevState.Buttons.A == ButtonState.Released && input.State.Buttons.A == ButtonState.Pressed) {
            if (!lastBeatWave || !lastBeatWave.CanHold()) {
                beatHoldAccumulator = 0;
            } else {
                lastBeatWave.Hold();
                ++beatHoldAccumulator;
            }
        }*/

        _Direction = new Vector2(
            input.State.ThumbSticks.Left.X, 
            input.State.ThumbSticks.Left.Y);

        float multip = 1.0f;
        if (_GhostTime > 0)
            multip = 0.5f;

        MoveInDirection(_Direction, _PlayerSpeed * multip);        
    }

    public bool MoveInDirection(Vector2 direction, float velocity) {
        if (CurrHp <= 0) return true;

        direction.Normalize();
        Vector2 deltaPos = direction * velocity * Time.smoothDeltaTime;
        Vector2 newPosition = Position + deltaPos;

        //sliding along edges
        if (!CollisionUtils.IsCircleInRange(newPosition, HitBoxRadius)) {
            int dir = CollisionUtils.CircleInRectRange(newPosition, HitBoxRadius);
            if (dir == 1)
                deltaPos -= new Vector2(deltaPos.x, 0);
            if(dir == 2)                
                deltaPos -= new Vector2(0, deltaPos.y);

            newPosition = Position + deltaPos;

            // if still collide with edge, fail
            if(!CollisionUtils.IsCircleInRange(newPosition, HitBoxRadius))
                return false;            
        }

        //check for collisions
        if (_GhostTime < 0) {
            SpriteRenderer.color = _RegularColor;

            InPlaceArray<GameEntity> entities = GameController.Instance.Game.EntitiesCollidingWithCircle(newPosition, HitBoxRadius);
            for (int i = 0; i < entities.Count; i++) {
                GameEntity e = entities.Get(i);
                if (e.gameObject == this.gameObject) continue;


                if (e is EnemyBase) {
                    EnemyBase enemy = (EnemyBase)e;
                    if (!enemy.IsDestroyed)
                        DealDamageEnemy(enemy);                    
                }
                if (e is Bullet) {
                    Bullet bullet = (Bullet)e;
                    DealDamageBullet(bullet);
                }
                //handle collision

                return false;
            }
        } else {
            _GhostTime -= Time.deltaTime;
        }

        Position = newPosition;        
        return true;
    }

    void Update() {
    }

    private void SpawnBeatWave(Beat beat) {
        if (beat.Type != AudioChannel.BeatType.Bass) return;
        if (_GhostTime > 0) return;

        lastBeatWave = beatWaveFactory.Create(
            Position, 
            beat.BColor);
    }

    private void OnDamageReceived(int amount) {
        if (playerWasDad) return;

        PlaySound sound = new PlaySound();
        sound.Clip = hitSound;
        this.Signal<PlaySound>(sound);

        if (CurrHp <= 0) {
            playerWasDad = true;
            this.Signal<PlayerDied>(new PlayerDied());
            

            DOTween.Sequence()
                   // TODO: death animation
                   .AppendInterval(2.0f)
                   .AppendCallback(() => this.Signal(new GameOver(score)));
        } else {
            _GhostTime = _GhostingTime;
            this.Signal<Shake>(new Shake());
        }

        var damage = new PlayerDamaged();
        damage.Max = _InitialHp;
        damage.Current = CurrHp; 
        this.Signal(damage);
    }

    private void DealDamageEnemy(EnemyBase enemy) {
        if (enemy.IsDestroyed) return;

        CurrHp -= 1;
        enemy.CollidedWithPlayer();
    }

    private void DealDamageBullet(Bullet bullet) {
        CurrHp -= 1;
        bullet.CollidedWithPlayer();
    }

    [MessageSlot]
    void OnBeat(Beat beat) {
        if (CurrHp <= 0) return;
        if (beat.Type == AudioChannel.BeatType.None) return;

        if (!lastBeatWave) {
            SpawnBeatWave(beat);
        }
    }

    [MessageSlot]
    void OnScoreGained(ScoreGained scoreGained) {
        if (CurrHp > 0) {
            score += scoreGained.Value;
        }
    }

    void LateUpdate() {        
        this.Signal(PlayerPositionChanged.GetInstance(transform.position));
    }

    public class Factory : Factory<PlayerController> { }
}
