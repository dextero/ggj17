﻿using Moonlit.Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(EnemyBase))]
public class FloweryShooter : EnemyBase.EnemyAttack, IMessageSender {
    #region Config
    [SerializeField]
    protected float _BulletVelocity = 100;
    [SerializeField]
    protected float _ShootDeltaTime = 0.1f;

    [SerializeField]
    protected int _NumberOfSequences = 5;
    [SerializeField]
    protected AudioClip roarSound;
    #endregion

    private bool _PerformingAttack = false;

    public override void PerformAttack() {
        if (EnemyBaseCache.IsDestroyed) return;
        StartCoroutine(AttackCoroutine());       
    }

    IEnumerator AttackCoroutine() {
        if (EnemyBaseCache.EnemyMovementComponent != null)
            EnemyBaseCache.EnemyMovementComponent.StopMovement = true;

        yield return new WaitForSeconds(0.5f);

        GameController.BeatStation randomColor
            = Utilities.RandomEnum<GameController.BeatStation>();

        PlaySound sound = new PlaySound();
        sound.Clip = roarSound;
        this.Signal<PlaySound>(sound);

        float extraDelta = 0.0f;
        _NumberOfSequences = 5 + UnityEngine.Random.Range(0, 10);
        for (int i = 0; i < _NumberOfSequences; i++) {
            yield return new WaitForSeconds(_ShootDeltaTime);

            this.Signal(MessageShootBullet.Instance(EnemyBaseCache.Position, 0 + extraDelta, _BulletVelocity, randomColor));
            this.Signal(MessageShootBullet.Instance(EnemyBaseCache.Position, 90 + extraDelta, _BulletVelocity, randomColor));
            this.Signal(MessageShootBullet.Instance(EnemyBaseCache.Position, 180 + extraDelta, _BulletVelocity, randomColor));
            this.Signal(MessageShootBullet.Instance(EnemyBaseCache.Position, 270 + extraDelta, _BulletVelocity, randomColor));
            
            extraDelta += 45;
        }
            
        yield return new WaitForSeconds(1.0f);

        EnemyBaseCache.EnemyMovementComponent.StopMovement = false;
    }    
}
