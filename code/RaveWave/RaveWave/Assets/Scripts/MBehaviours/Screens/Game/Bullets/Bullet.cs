﻿using Moonlit.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

public class Bullet : GameEntity, IMessageReceiver {

    #region Configuration
    [SerializeField]
    protected float _Velocity;    
    #endregion

    [SerializeField]
    protected SpriteRenderer _BulletSprite;

    // from right
    protected float _Direction;
    private Vector2 _DirectionVec;
    public GameController.BeatStation CType;

    public float SineDirection = 0.0f;    
    public float SineTimer = 0.0f;
    public float SineDelta;
    private float TimeSinceLastBounceBack = 0.0f;

    public float Direction {
        get { return _Direction; }
        set {
            _Direction = value;
            _DirectionVec = Vector2.right.Rotate(_Direction);
        }
    }

    public float Velocity {
        get { return _Velocity; }
        set { _Velocity = value; }
    }

    public void Initialise(MessageShootBullet msg) {
        Position = msg.StartPosition;
        Direction = msg.Direction;
        Velocity = msg.Velocity;
        CType = msg.ColorType;

        _BulletSprite.color 
            = GameController
                .Instance
                .GetColorForBeatStation(CType);

        if (msg.SineWaveDelta != 0) {
            SineTimer = 0.0f;
            SineDelta = 0.0f;
            SineDirection = msg.SineWaveDelta;
        } else {
            SineDirection = 0.0f;            
            SineTimer = 0.0f;
        }
    }

    protected void Update() {
        Vector2 _SinDirVew = new Vector2(_DirectionVec.x, _DirectionVec.y);

        //ADD SINE
        if (SineDirection != 0.0f) {
            SineTimer += Time.deltaTime * 1.0f;
            SineDelta = 50.0f;            
            _SinDirVew = _SinDirVew.Rotate(Mathf.Sin(Mathf.PI * (SineTimer)) * SineDelta * SineDirection);
        }

        Position = Position + _SinDirVew * Velocity * Time.smoothDeltaTime;

        _BulletSprite.transform.localRotation = Quaternion.Euler(0, 0, _SinDirVew.Angle360(Vector2.right));        

        // kill when leave screen
        if (!CollisionUtils.IsCircleInRange(Position, HitBoxRadius)) {
            this.gameObject.SetActive(false);
        }

        if (TimeSinceLastBounceBack > 0)
            TimeSinceLastBounceBack -= Time.deltaTime;
    }

    protected void OnEnable() {
        this.Register();
    }

    protected void OnDisable() {
        this.Unregister();
    }

    public void CollidedWithPlayer() {
        this.gameObject.SetActive(false);

    }

    [MessageSlot]
    private void Receive(PlayerDied msg) {
        CollidedWithPlayer();
    }

    public void BounceBack(Vector2 centerPosition) {        
        if (TimeSinceLastBounceBack > 0) return;

        TimeSinceLastBounceBack = 0.5f;

        Direction = (Position - centerPosition).Angle360(Vector2.right);

        //disable sine stuff
        SineDirection = 0.0f;
    }

    public class BasicBullet : Factory<Bullet> { }
}
