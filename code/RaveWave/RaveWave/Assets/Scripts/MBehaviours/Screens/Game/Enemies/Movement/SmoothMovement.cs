﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(EnemyBase))]
public class SmoothMovement : EnemyBase.EnemyMovement {    

    public override bool MoveInDirection(Vector2 direction, float velocity) {
        if (EnemyBaseCache.IsDestroyed) return true;
        Direction = direction;
        direction.Normalize();
        Vector2 newPosition = EnemyBaseCache.Position + direction * velocity * Time.smoothDeltaTime;


        if(!CollisionUtils.IsCircleInRange(newPosition, EnemyBaseCache.HitBoxRadius)) 
            return false;
        
        /*
        InPlaceArray<GameEntity> entities = GameController.Instance.Game.EntitiesCollidingWithCircle(newPosition, EnemyBaseCache.HitBoxRadius);
        for (int i = 0; i < entities.Count; i++) {
            GameEntity e = entities.Get(i);
            if (e.gameObject == this.gameObject) continue;
            // bump with player all you want
            if (e is PlayerController) continue;

            return false;
        }            
        */

        EnemyBaseCache.Position = newPosition;        
        return true;
    }
}

