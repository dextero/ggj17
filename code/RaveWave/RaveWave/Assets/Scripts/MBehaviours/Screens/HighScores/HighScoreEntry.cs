using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Entry {
    public string nickname;
    public int score;

    [Inject]
    void Inject() { }

    public Entry(string nickname,
                 int score) {
        this.nickname = nickname;
        this.score = score;
    }

    public string ToString() {
        return string.Format("{0} {1}", nickname, score);
    }

    public static Entry Unnamed(int score) {
        return new Entry("", score);
    }

    public static Entry Placeholder {
        get { return new Entry("AAA", 0); }
    }
}

public class HighScoreEntry: MonoBehaviour {
    private Text __nicknameComponent;
    private Text NicknameComponent {
        get {
            if (__nicknameComponent == null) {
                __nicknameComponent = transform.FindChild("name").GetComponent<Text>();
            }
            return __nicknameComponent;
        }
    }

    private Text __scoreComponent;
    private Text ScoreComponent {
        get {
            if (__scoreComponent == null) {
                __scoreComponent = transform.FindChild("score").GetComponent<Text>();
            }
            return __scoreComponent;
        }
    }

    [Inject]
    void Inject(Entry entry) {
        NicknameComponent.text = entry.nickname;
        ScoreComponent.text = entry.score.ToString("D9");
    }

    void Start() {
    }

    public class Factory : Factory<Entry, HighScoreEntry> {}
}