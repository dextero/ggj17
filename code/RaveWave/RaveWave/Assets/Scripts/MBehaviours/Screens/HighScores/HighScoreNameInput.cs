﻿using System;
using Moonlit.Messages;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;
using Zenject;

[Serializable] public class APress {}
[Serializable] public class BPress {}

[Serializable]
public class NicknameSubmitted {
    public string Value;
}

public class HighScoreNameInput : AScreen, IMessageReceiver, IMessageSender {
    private int currentLetterIdx = 0;

    private Text[] _Text;
    private Text Text {
        get {
            if (_Text == null) {
                _Text = new[] {
                    transform.FindChild("letter0").GetComponent<Text>(),
                    transform.FindChild("letter1").GetComponent<Text>(),
                    transform.FindChild("letter2").GetComponent<Text>(),
                };
            }
            return _Text[currentLetterIdx];
        }
    }

    [Inject] protected IInputHandler input;

    [Inject]
    void Inject() {
        Text.text = "A";
        currentLetterIdx = 0;
    }

	void Start () {
        this.Register();
	}

    void OnDestroy() {
        this.Unregister();
    }

    private void Submit() {
        string name = _Text[0].text + _Text[1].text + _Text[2].text;
        this.Signal(new NicknameSubmitted() { Value = name });
    }

    void FixedUpdate () {
	    if (input.PrevState.Buttons.B == ButtonState.Released && input.State.Buttons.B == ButtonState.Pressed) {
            OnBPRess(null);
	    }

	    if ((input.PrevState.Buttons.A == ButtonState.Released && input.State.Buttons.A == ButtonState.Pressed)
            || (input.PrevState.Buttons.RightStick == ButtonState.Released && input.State.Buttons.RightStick == ButtonState.Pressed)
            || (input.PrevState.Buttons.RightShoulder == ButtonState.Released && input.State.Buttons.RightShoulder == ButtonState.Pressed)) {
            OnAPress(null);
	    }
	}

    [MessageSlot]
    void OnAPress(APress a) {
        if (++currentLetterIdx >= 3) {
            Submit();
            currentLetterIdx = 2;
        } else {
            Text.text = "A";
        }
    }

    [MessageSlot]
    void OnBPRess(BPress b) {
        Text.text = "";
        currentLetterIdx = Math.Max(0, currentLetterIdx - 1);
    }

    [MessageSlot]
    void OnSetFrequency(SetFrequency freq) {
        char c = (char)('A' + Mathf.FloorToInt(freq.Value * ((float)'Z' - (float)'A' + 1.0f) / 100.0f));
        Text.text = "" + c;
    }
}