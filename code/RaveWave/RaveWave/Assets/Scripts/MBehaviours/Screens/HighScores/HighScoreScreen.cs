﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DG.Tweening;
using ModestTree;
using Moonlit.Messages;
using UnityEngine;
using XInputDotNetPure;
using Zenject;

public class HighScoreScreen : AScreen, IMessageReceiver {
    const string HIGHSCORE_FILE = "highscores.txt";
    const int MAX_HIGHSCORES = 10;

    [Inject] private HighScoreEntry.Factory entryFactory;
    [Inject] protected IInputHandler input;

    private bool isInputEnabled = false;

    private static Entry LineToEntry(string line) {
        string[] words = line.Trim().Split(' ');
        return new Entry(words[0], int.Parse(words[1]));
    }

    private static List<Entry> LoadFromFile(string path) {
        try {
            return File.ReadAllLines(path)
                       .Select(LineToEntry)
                       .ToList();
        }
        catch (FileNotFoundException e) {
            return new List<Entry>();
        }
    }

    private void SaveToFile(string path, List<Entry> entries) {
        File.WriteAllLines(path, entries.Select(e => e.ToString()).ToArray());
    }

    private void Reload(List<Entry> entries) {
        foreach (HighScoreEntry e in transform.GetComponentsInChildren<HighScoreEntry>()) {
            Destroy(e);
        }

        List<Entry> placeholders = new List<Entry>();
        for (int i = 0; i < MAX_HIGHSCORES; ++i) {
            placeholders.Add(Entry.Placeholder);
        }

        foreach (Entry e in entries.OrderByDescending(e => e.score).Concat(placeholders).Take(MAX_HIGHSCORES)) {
            entryFactory.Create(e).transform.SetParent(transform);
        }
    }

    public static bool IsHighScore(int score) {
        List<Entry> entries = LoadFromFile(HIGHSCORE_FILE);
        int lowestScore = entries.OrderByDescending(e => e.score)
                                 .Append(Entry.Placeholder)
                                 .Take(MAX_HIGHSCORES)
                                 .Last()
                                 .score;
        return score > lowestScore;
    }

    [Inject]
    void Inject(Entry newEntry) {
        List<Entry> entries = LoadFromFile(HIGHSCORE_FILE);

        if (newEntry != null) {
            entries.Add(newEntry);
            SaveToFile(HIGHSCORE_FILE, entries);
        }

        Reload(entries);

        // FUCK THIS
        isInputEnabled = false;
        DOTween.Sequence().AppendInterval(1.0f).AppendCallback(() => isInputEnabled = true) ;

    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (isInputEnabled && (input.State.Buttons.A == ButtonState.Pressed && input.PrevState.Buttons.A == ButtonState.Released
            || input.State.Buttons.Start == ButtonState.Pressed && input.PrevState.Buttons.Start == ButtonState.Released
            || Input.anyKeyDown || Input.GetMouseButtonDown(0))) {
            ShowMenu();
	    }
    }

    public void ShowMenu() {
        GameController.Instance.ShowScreen(GameController.ScreenTypes.MainMenu);
    }

    public class Factory : Factory<Entry, HighScoreScreen> { }
}
