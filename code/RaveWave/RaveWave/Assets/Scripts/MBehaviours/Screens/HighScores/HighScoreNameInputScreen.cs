﻿using Moonlit.Messages;
using UnityEngine.UI;
using XInputDotNetPure;
using Zenject;

public class HighScoreNameInputScreen : AScreen, IMessageReceiver {
    [Inject] protected HighScoreNameInput nameInput;

    private Text _ScoreText;
    private Text ScoreText {
        get {
            if (_ScoreText == null) {
                _ScoreText = transform.FindChild("score").GetComponent<Text>();
            }
            return _ScoreText;
        }
    }

    private Text _ScoreHeaderText;
    private Text ScoreHeaderText {
        get {
            if (_ScoreHeaderText == null) {
                _ScoreHeaderText = transform.FindChild("header").GetComponent<Text>();
            }
            return _ScoreHeaderText;
        }
    }

    private Entry entryToFill;
    private bool isHighScore;

    [Inject] protected IInputHandler input;

    [Inject]
    void Inject(Entry entryToFill) {
        this.entryToFill = entryToFill;
        this.isHighScore = HighScoreScreen.IsHighScore(entryToFill.score);

        ScoreText.text = entryToFill.score.ToString();
        ScoreHeaderText.text = isHighScore ? "New high score!" : "You scored:";

        nameInput.transform.SetParent(transform);
        nameInput.gameObject.SetActive(isHighScore);
    }

    void Start() {
        this.Register();
    }

    void Update() {
        // herp derp: in case of actual hiscores it's handled by HighScoreNameInput
        if (!isHighScore
	        && ((input.PrevState.Buttons.A == ButtonState.Released && input.State.Buttons.A == ButtonState.Pressed)
                || (input.PrevState.Buttons.RightStick == ButtonState.Released && input.State.Buttons.RightStick == ButtonState.Pressed)
                || (input.PrevState.Buttons.RightShoulder == ButtonState.Released && input.State.Buttons.RightShoulder == ButtonState.Pressed)))
        {
            GameController.Instance.ShowScreen(GameController.ScreenTypes.HighScoreMenu);  
        }
    }

    void OnDestroy() {
        this.Unregister();
    }

    [MessageSlot]
    void OnNicknameSubmitted(NicknameSubmitted nick) {
        entryToFill.nickname = nick.Value;
        GameController.Instance.ShowScreen(GameController.ScreenTypes.HighScoreMenu, entryToFill);  
    }

    public class Factory : Factory<Entry, HighScoreNameInputScreen> { }
}
