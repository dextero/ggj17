﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Splash : MonoBehaviour {
    [SerializeField]
    protected Image _Background;
    [SerializeField]
    protected RectTransform _Logo;

    public Action Callback;
    private bool transitionrunning = false;
    int firstTime = 2;

    protected void Awake() {
        _Background.color = new Color(0, 0, 0, 0.0f);
        _Logo.localScale = Vector3.zero;
    }

    [ContextMenu("ANIM!")]
    public void StartAnimation() {
        if (transitionrunning) return;
        transitionrunning = true;

        _Background.color = new Color(0, 0, 0, 0.0f);
        _Logo.localScale = Vector3.zero;
        _Logo.GetComponent<Image>().color = Color.white;        
        
        DOTween
            .Sequence()
            .Insert(0.0f,
                DOTween
                    .Sequence()
                    .Insert(0.0f, _Logo
                        .DOScale(firstTime > 0 ? 0.0f : 1.0f, 0.4f)
                        .SetEase(Ease.InCubic))
                    .Insert(0.30f, _Background
                        .DOColor(new Color(0, 0, 0, 1.0f), 0.10f)
                        .SetEase(Ease.InSine))
            )
            .InsertCallback(1.0f, () => {                
                if (Callback != null) {
                    Callback();
                }                

                  DOTween
                    .Sequence()
                    .Insert(0.0f + 0.5f, _Logo
                        .DOScale(0.0f, 0.3f)
                        .SetEase(Ease.OutCubic))
                    .Insert(0.25f + 0.5f, _Logo
                        .GetComponent<Image>()
                        .DOFade(0.0f, 0.05f))
                        .Insert(firstTime > 0 ? 0.0f : (0.0f + 0.5f), _Background
                        .DOColor(new Color(0, 0, 0, 0.0f), firstTime > 0 ? 1.0f : 0.3f)
                        .SetEase(Ease.OutCubic))
                        .AppendCallback(() => {
                            transitionrunning = false;
                            firstTime--;                            
                        }); 
            });           
    }
}
