﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;
using Zenject;


public class Story : AScreen {

    [SerializeField]
    Image targetImage;

    [SerializeField]
    Sprite[] comic;

    [Inject]
    protected IInputHandler input;

    int counter = -1;

    private float timer = 3.0f;

    protected void Update() {
        timer += Time.deltaTime;

        if ((input.PrevState.Buttons.A == ButtonState.Pressed && input.State.Buttons.A == ButtonState.Released)
           || (input.PrevState.Buttons.RightShoulder == ButtonState.Pressed && input.State.Buttons.RightShoulder == ButtonState.Released)
           || (input.PrevState.Buttons.RightStick == ButtonState.Pressed && input.State.Buttons.RightStick == ButtonState.Released)) {
            timer = 999999.0f;
        }

        if (counter < comic.Length && timer > 3.0f) {
            timer = 0.0f;
            if (counter >= comic.Length) {
                GameController.Instance.ShowScreen(GameController.ScreenTypes.MainMenu);
                return;
            }

            targetImage.sprite = comic[(int)Math.Max(0, counter)];
            targetImage.DOColor(Color.white, 1.0f)
                .OnComplete(() => {
                    timer = 0.0f;
                    counter++;
                    if (counter >= comic.Length) {

                        GameController.Instance.ShowScreen(GameController.ScreenTypes.MainMenu);

                    } else {
                        targetImage.sprite = comic[counter];
                        targetImage.DOFade(1.0f, 1.0f)
                            .OnComplete(() => {
                                timer = 0.0f;
                            });
                    }
                });
            
            
        }
        
    }

    public class Factory : Factory<Story> { }
}

