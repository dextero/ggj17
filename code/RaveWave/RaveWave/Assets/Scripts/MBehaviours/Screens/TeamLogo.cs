﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;


public class TeamLogo : AScreen {

    public class Factory : Factory<TeamLogo> { }

    private float timer = 0.0f;

    protected void Update() {
        timer += Time.deltaTime;
        if (timer > 2.0f) {
            GameController.Instance.ShowScreen(GameController.ScreenTypes.Story);
        }
    }
}

