﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
[RequireComponent(typeof(AudioSource))]
public class AudioChannel : MonoBehaviour {

    #region Public Types
    [Serializable]
    public enum BeatType
    {
        None,
        Bass
    }
    [Serializable]
    public class BeatData
    {
        public int Begin;
        public int Period;
        public BeatType Type;
    }
    #endregion

    #region Public Variables
    [HideInInspector] public AudioSource Source;

    [Header("Frequency")]
    public float StartFrequency;
    public float EndFrequency;

    [Header("Beat")]
    public int BPM;
    public List<BeatData> Data;
    #endregion

    #region Unity Messages
    private void Awake()
    {
        Source = GetComponent<AudioSource>();
    }
    #endregion

    #region Public Methods
    public int GetBeatTempo(float time, int tempo = 32)
    {
        return (int)(time * BPM * (float)(tempo) / 60.0f)  % BPM;
    }

    public BeatType GetBeatAtSecond(float time)
    {
        int tempo   = GetBeatTempo(time, 8);
        var beat = Data.Find(x => 
        {
            return x.Begin == (tempo % x.Period);
        });

        if(beat == null)
        {
            return BeatType.None;
        }

        return beat.Type;
    }
    #endregion
}
