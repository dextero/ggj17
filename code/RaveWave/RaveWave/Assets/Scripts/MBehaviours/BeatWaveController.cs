﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class BeatWaveController : GameEntity {
    #region Config
    [SerializeField]
    protected float SECONDS_TO_HOLD_BEAT = 0.2f;
    [SerializeField]
    protected float spreadDuration = 1.0f;
    [SerializeField]
    protected float maxRadius = 3;
    [SerializeField]
    protected float fadeOutTime = 1.0f;
    [SerializeField]
    protected float stopKillingSecondsBeforeDeath = 0.3f;  
    #endregion    
    
    public override float HitBoxRadius {
        set {
            _HitBoxRadius = value;
            _Wave.Radius = 0.015f * _HitBoxRadius;                                    
        }
        get { return _HitBoxRadius; }
    }

    private float secondsSinceSpawn;
    [SerializeField]
    private SoundWave _Wave;
    [SerializeField]
    private SpriteRenderer _WaveSprite;
    private bool isHeld = false;
    private GameController.BeatStation _BeatType;
    private bool ToBeKilled = false;
    private bool Disable = false;
    EaseFunction spreadFunction;

    public bool IsHeld { get { return isHeld; } }
    public GameController.BeatStation BeatType {
        get { return _BeatType; }
    }

    InPlaceArray<Vector2> _Ranges = new InPlaceArray<Vector2>();

    [Inject]
    public void Construct(
            Vector2 startPosition, 
            GameController.BeatStation type) {

        spreadFunction = AnimationUtils.GetBeizerEasing(0.15f, 0.47f, 0.09f, 0.97f);
        Position = startPosition;
        HitBoxRadius = 0.0f;
        secondsSinceSpawn = 0.0f;
        _BeatType = type;
        
        _Wave.Color = GameController.Instance.GetColorForBeatStation(_BeatType);        
        GameController.Instance.GetRangesForBeatType(_BeatType, _Ranges);    
    
        _Wave._Ranges = new Vector2[_Ranges.Count];
        for (int i = 0; i < _Ranges.Count; i++)
            _Wave._Ranges[i] = new Vector2(
                _Ranges.Get(i).x, 
                _Ranges.Get(i).x + _Ranges.Get(i).y);
    }

    public bool CanHold() {
        return (secondsSinceSpawn) < SECONDS_TO_HOLD_BEAT;
    }

    public void Hold() {
        isHeld = true;
    }    

    void Update() {
        if (Disable) return;
        secondsSinceSpawn += Time.deltaTime;

        if (isHeld && CanHold()) {
            //TODO: notify that the big was hit     
            Disable = true;
            KillAnim();
            return;            
        }        

        float spreadPercent = spreadFunction(secondsSinceSpawn, spreadDuration, 0.0f, 0.0f);        

        HitBoxRadius = spreadPercent * maxRadius;

        if (spreadDuration - secondsSinceSpawn < fadeOutTime)
            KillAnim();

        if (spreadDuration - secondsSinceSpawn < stopKillingSecondsBeforeDeath)
            return;

        if(GameController.Instance.Game != null) {
            InPlaceArray<GameEntity> entities = 
                GameController.Instance.Game.EntitiesAtCircleBorder(Position, HitBoxRadius, 1, _Ranges);

            for (int i = 0; i < entities.Count; i++) {
                GameEntity entity = entities.Get(i);

                // kill enemy
                if (entity is EnemyBase) {                    
                    EnemyBase enemy = (EnemyBase)entities.Get(i);
                    if(!enemy.IsDestroyed)
                        enemy.KillByWave(this);                    
                }

                // bounce back
                if (entity is Bullet) {
                    Bullet bullet = (Bullet)entity;
                    if (GameController.AreBeatsEqual(bullet.CType, BeatType)) {
                        bullet.BounceBack(Position);
                    }
                }
            }
            entities.Clear();
        }                
    }

    private void KillAnim() {
        if (ToBeKilled) return;
        ToBeKilled = true;
        
        _WaveSprite.color = _Wave.Color;
        _WaveSprite.DOFade(0.0f, fadeOutTime)
            .OnUpdate(() =>{
                _Wave.Color = _WaveSprite.color;
            })
            .OnComplete(() => {
                Destroy(this.gameObject);
            });
    }

    public class Factory: Factory<Vector2, GameController.BeatStation, BeatWaveController> {}
}
