﻿using Moonlit.Messages;
using UnityEngine;
using XInputDotNetPure;
using Zenject;

public class FrequencyDialController: MonoBehaviour, IMessageSender {
    #region Configuration
    [SerializeField]
    protected float _DefaultRadioFrequency = 5.0f;
    [SerializeField]
    protected float _MinRadioFrequency = 0.0f;
    [SerializeField]
    protected float _MaxRadioFrequency = 100.0f;
    [SerializeField]
    protected float _RadioDialFullTurnsToCoverFullFreqRange = 1.0f;
    // rotations faster than this per frame will be handled
    // as if the player released the stick and started rotating again
    [SerializeField]
    protected float _MaxSingleFrameAngleDiff = Mathf.PI / 8.0f;
    #endregion

    protected float _RadioFrequencyDiffPerDialTurn;

    [Inject] protected IInputHandler input;

    private float prevRadioFreqDialAngle = 0.0f;
    private float currFrequency = 0.0f;

    [Inject]
    void Inject() {
        currFrequency = _DefaultRadioFrequency;

        _RadioFrequencyDiffPerDialTurn =
            (_MaxRadioFrequency - _MinRadioFrequency) / _RadioDialFullTurnsToCoverFullFreqRange;
    }

    public void FixedUpdate() {
        float currRadioFreqDialAngle = Mathf.Atan2(input.State.ThumbSticks.Right.Y, input.State.ThumbSticks.Right.X);
        float angleDiff = (Mathf.PI * 3.0f + (currRadioFreqDialAngle - prevRadioFreqDialAngle)) % (Mathf.PI * 2.0f) - Mathf.PI;

        if (Mathf.Abs(angleDiff) <= _MaxSingleFrameAngleDiff) {
            // negating angleDiff ensures that clockwise motion corresponds to increasing frequency
            float freqDiff = _RadioFrequencyDiffPerDialTurn * -angleDiff / (Mathf.PI * 2.0f);
            float freqRange = _MaxRadioFrequency - _MinRadioFrequency;

            currFrequency = (currFrequency + freqDiff - _MinRadioFrequency + freqRange)%freqRange +
                            _MinRadioFrequency;

            this.Signal(new SetFrequency {Value = currFrequency});
        }

        prevRadioFreqDialAngle = currRadioFreqDialAngle;
    }
}
