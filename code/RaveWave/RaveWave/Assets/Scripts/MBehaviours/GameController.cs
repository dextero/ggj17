﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using ModestTree;
using UnityEngine.UI;
using Zenject;
using Moonlit.Messages;

public class GameController : Singleton<GameController>, IMessageReceiver {
    public enum BeatStation {
        NONE,
        RED,
        OTHER_RED,
        OTHER_BLUE,
        BLUE
    }

    public enum ScreenTypes {
        MainMenu,
        Story,
        HighScoreMenu,
        GameScreen,
        TeamLogo
    }

    #region Game Config
    [SerializeField]
    protected Vector2 _GameplayMapSize;
    [Header("Colors")]
    [SerializeField]
    protected Color _RedColor;
    [SerializeField]
    protected Color _GreenColor;
    [SerializeField]
    protected Color _YellowColor;
    [SerializeField]
    protected Color _BlueColor;
    [SerializeField]
    protected CanvasScaler scaler;

    [Header("Ranges")]
    [SerializeField]
    protected List<Vector2> _RedColorRanges;
    [SerializeField]
    protected List<Vector2> _GreenColorRanges;
    [SerializeField]
    protected List<Vector2> _YellowColorRanges;
    [SerializeField]
    protected List<Vector2> _BlueColorRanges;
    #endregion

    #region Screen Factories
    [Inject]
    private GameScreen.Factory _GameScreenFactory;

    [Inject]
    private MenuScreen.Factory _MenuScreenFactory;

    [Inject]
    private TeamLogo.Factory _TeamLogoFactory;

    [Inject]
    private HighScoreScreen.Factory _HighScoreScreenFactory;

    [Inject]
    private Story.Factory _StoryFactory;

    [Inject]
    private HighScoreNameInputScreen.Factory _HighScoreNameInputScreenFactory;

    [Inject]
    private Splash _Transition;
    #endregion

    #region Properties
    public Vector2 GameplayMapSize {
        get { return _GameplayMapSize; }
    }

    public GameScreen Game {
        get {
            if (!(_ActiveScreen is GameScreen))
                return null;
            return (GameScreen)_ActiveScreen;
        }
    }
    #endregion

    AScreen _ActiveScreen;

    private void DestroyAllChildObjects(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; --i) {
            Destroy(parent.GetChild(i).gameObject);
        }
    }

    public void ShowScreen(ScreenTypes types, Entry hiscoreEntry = null) {
        _Transition.Callback = () => {

            // destroy previous game object
            if (_ActiveScreen != null) {
                DestroyImmediate(_ActiveScreen.gameObject);
                DestroyAllChildObjects(transform.Find("/scene_root"));
                DestroyAllChildObjects(transform.Find("/ui_root"));
            }

            switch (types) {
            case ScreenTypes.GameScreen:
                scaler.referenceResolution = new Vector2(600, 400);
                _ActiveScreen = _GameScreenFactory.Create();
                    
                break;
            case ScreenTypes.MainMenu:
                scaler.referenceResolution = new Vector2(1000, 900);
                _ActiveScreen = _MenuScreenFactory.Create();
                break;
			case ScreenTypes.TeamLogo:
                    _ActiveScreen = _TeamLogoFactory.Create();
                    break;
            case ScreenTypes.HighScoreMenu:
                    scaler.referenceResolution = new Vector2(1000, 900);
                if (hiscoreEntry != null && hiscoreEntry.nickname.IsEmpty()) {
                    _ActiveScreen = _HighScoreNameInputScreenFactory.Create(hiscoreEntry);
                } else {
                    _ActiveScreen = _HighScoreScreenFactory.Create(hiscoreEntry);
                }
                break;
            case ScreenTypes.Story:
                _ActiveScreen = _StoryFactory.Create();
                break;
            }

        };

        _Transition.StartAnimation();
    }

    [Inject] protected IInputHandler input;

    // Use this for initialization
    void Start() {
        Debug.Log("Registering");
        this.Register();

        ShowScreen(ScreenTypes.TeamLogo);
//        ShowScreen(ScreenTypes.HighScoreMenu, Entry.Unnamed(1500));
    }

    void FixedUpdate() {
        input.OnFixedUpdate();
    }

    // Update is called once per frame
    void Update() {
        input.OnUpdate();
    }
    
    [MessageSlot]
    void OnGameOver(GameOver gameOver) {
        ShowScreen(ScreenTypes.HighScoreMenu, Entry.Unnamed(gameOver.FinalScore));
    }

    public Color GetColorForBeatStation(GameController.BeatStation type) {
        switch (type) {
            case BeatStation.BLUE:
                return _BlueColor;                
            case BeatStation.OTHER_RED:
                return _GreenColor;                
            case BeatStation.RED:
                return _RedColor;                
            case BeatStation.OTHER_BLUE:
                return _YellowColor;                
        }
        return Color.grey;
    }

    public void GetRangesForBeatType(BeatStation type, InPlaceArray<Vector2> range) {
        range.Clear();

        List<Vector2> ranges = null;

        switch (type) {
            case BeatStation.BLUE:
                ranges = _BlueColorRanges;
                break;
            case BeatStation.OTHER_RED:
                ranges = _GreenColorRanges;
                break;
            case BeatStation.RED:
                ranges = _RedColorRanges;
                break;
            case BeatStation.OTHER_BLUE:
                ranges = _YellowColorRanges;
                break;
        }

        if(ranges != null)
            for (int i = 0; i < ranges.Count; i++)
                range.Add(ranges[i]);
    }

    public static bool AreBeatsEqual(BeatStation a, BeatStation b) {
        if(b == BeatStation.OTHER_BLUE)
            b = BeatStation.BLUE;
        if(a == BeatStation.OTHER_BLUE)
            a = BeatStation.BLUE;
        if(b == BeatStation.OTHER_RED)
            b = BeatStation.RED;
        if(a == BeatStation.OTHER_RED)
            a = BeatStation.RED;

        return a == b;
    }
}
