﻿using System;
using System.Reflection;
using UnityEngine;
using XInputDotNetPure;

public interface IInputHandler {
    GamePadState State { get; }
    GamePadState PrevState { get; }

    void OnFixedUpdate();
    void OnUpdate();
}

public class KeyboardInputHandler: IInputHandler {
    private GamePadState state;
    public GamePadState State { get { return state; } }

    private GamePadState prevState;
    public GamePadState PrevState { get { return prevState; } }

    public void OnFixedUpdate() {
        prevState = state;

        Type type = typeof(GamePadButtons);
        GamePadButtons buttons = (GamePadButtons)typeof (GamePadButtons)
            .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[0]
            .Invoke(new object[] {
                ButtonState.Released, // Start
                ButtonState.Released, // Back
                ButtonState.Released, // LeftStick
                ButtonState.Released, // RightStick
                ButtonState.Released, // LeftShoulder
                ButtonState.Released, // RightShoulder
                ButtonState.Released, // Guide
                Input.GetKey(KeyCode.Space) ? ButtonState.Pressed : ButtonState.Released, // A
                ButtonState.Released, // B
                ButtonState.Released, // X
                ButtonState.Released // Y
                });

        GamePadThumbSticks.StickValue leftStick = (GamePadThumbSticks.StickValue) typeof (GamePadThumbSticks.StickValue)
            .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[0]
            .Invoke(new object[] {
                (float)((Input.GetKey(KeyCode.D) ? 1 : 0) - (Input.GetKey(KeyCode.A) ? 1 : 0)),
                (float)((Input.GetKey(KeyCode.W) ? 1 : 0) - (Input.GetKey(KeyCode.S) ? 1 : 0))
            });

        float rx = 0.0f;
        float ry = 0.0f;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)) {
            float angle = Mathf.Atan2(prevState.ThumbSticks.Right.Y, prevState.ThumbSticks.Right.X);
            if (Input.GetKey(KeyCode.LeftArrow)) {
                angle -= 0.1f;
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                angle += 0.1f;
            }
            rx = Mathf.Cos(angle);
            ry = Mathf.Cos(angle);
        }

        GamePadThumbSticks.StickValue rightStick = (GamePadThumbSticks.StickValue) typeof (GamePadThumbSticks.StickValue)
            .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[0]
            .Invoke(new object[] { rx, ry });

        GamePadThumbSticks thumbSticks = (GamePadThumbSticks) typeof(GamePadThumbSticks)
            .GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[0]
            .Invoke(new object[] { leftStick, rightStick });

        // TODO: does not work at all
        state.GetType().GetField("isConnected", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).SetValue(state, true);
        state.GetType().GetField("buttons", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).SetValue(state, buttons);
        state.GetType().GetField("thumbSticks", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).SetValue(state, thumbSticks);
    }

    public void OnUpdate() {
    }
}

public class GamepadInputHandler: IInputHandler {
    private PlayerIndex playerIndex;

    private GamePadState state;
    public GamePadState State { get { return state; } }

    private GamePadState prevState;
    public GamePadState PrevState { get { return prevState; } }

    private void SetupController() {
        for (int i = 0; i < 4; ++i) {
            PlayerIndex testPlayerIndex = (PlayerIndex)i;
            GamePadState testState = GamePad.GetState(testPlayerIndex);

            if (testState.IsConnected) {
                Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                playerIndex = testPlayerIndex;
                break;
            }
        }
    }

    public void OnFixedUpdate() {
        prevState = state;
        state = GamePad.GetState(playerIndex);
    }

    public void OnUpdate() {
        if (!prevState.IsConnected) {
            SetupController();
        }
    }
}
