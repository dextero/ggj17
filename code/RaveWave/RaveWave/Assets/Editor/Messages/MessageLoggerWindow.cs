﻿

namespace Moonlit.Messages.Tools
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class MessageLoggerWindow : EditorWindow
    {
        #region Private Variables
        private MessageInspector _Controller = new MessageInspector();
        private GUIStyle _Style;

        private List<MessageDispatcher.MessageCallback> _Logs = 
            new List<MessageDispatcher.MessageCallback>();

        private Vector2 _ScrollViewPosition;
        #endregion Private Variables

        #region Unity Messages
        [MenuItem("Window/MessageLogger")]
        public static void ShowWindow()
        {
            var window = GetWindow<MessageLoggerWindow>();
            window.titleContent = new GUIContent("Logger");
            window.autoRepaintOnSceneChange = true;
            window._Style = new GUIStyle(EditorStyles.helpBox);

            MessageDispatcher.Callbacks += window.Callback;
        }

        public void OnGUI()
        {
            _ScrollViewPosition = EditorGUILayout.BeginScrollView(_ScrollViewPosition);

            int i = 0;
            foreach (var log in _Logs)
            {
                var index = "[" + i.ToString() + "]";
                var messageType = log.Message.GetType();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField(index + " " + log.Message.GetType().Name);

                if(!log.Sender.Equals(null))
                {
                    EditorGUILayout.ObjectField("Sender: ", log.Sender as Object, log.Sender.GetType(), true);
                }
                if(!log.Receiver.Equals(null))
                {
                    EditorGUILayout.ObjectField("Receiver: ", log.Receiver as Object, log.Sender.GetType(), true);
                }

                var editor = _Controller.GetTypeEditor(messageType);
                if(editor.target != null)
                {
                    _Controller.SetMessage(messageType, log.Message);
                    editor.DrawDefaultInspector();
                }

                EditorGUILayout.EndHorizontal();

                ++i;
            }
            EditorGUILayout.EndScrollView();
        }
        #endregion Unity Messages

        #region Private Methods
        private void Callback(MessageDispatcher.MessageCallback data)
        {
            _Logs.Add(data);
        }
        #endregion Private Methods
    }
}