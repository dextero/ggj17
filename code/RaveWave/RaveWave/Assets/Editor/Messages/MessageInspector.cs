﻿using System.Collections.Generic;
using UnityEngine;

namespace Moonlit.Messages.Tools
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;

    using UnityEditor;
    using ComponentData = Dictionary<Component, MessageInspector.PerComponentData>;

    public class MessageInspector  
    {
        #region Public Types
        public class PerComponentData
        {
            #region Data
            public bool Show;
            public Component Component;
            public List<PerMethodData> MethodData;
            #endregion 
        }
        public class PerMethodData
        {
            #region Data
            public bool Show;
            public MessageDispatcher.MethodData MethodData;
            #endregion Data

            #region  Public Variables
            public string MethodName
            {
                get
                {
                    return MethodData.Info.Name;
                }
            }
            public string MessageTypeName
            {
                get
                {
                    return MethodData.MessageType.Name;
                }
            }

            public DispatchMethod Dispatch
            {
                get
                {
                    var attribute = MethodData.Info.GetCustomAttributes(typeof(MessageSlotAttribute), true);
                    if(attribute == null || attribute.Length != 1)
                    {
                        return DispatchMethod.Unknown;
                    }

                    var messageSlotAttribute = attribute[0] as MessageSlotAttribute;
                    return messageSlotAttribute.Type;
                }
            }
            #endregion Public Variables
        }
        #endregion Public Types

        #region Public Variables
        public ComponentData Data
        {
            get
            {
                return _Data;
            }
        }
        #endregion Public Variables

        #region Private Variables
        private static string _AssemblyDllName = "MessageModule";
        private static string _ClassDllName    = "MessageClass";
        private static string _FieldName = "msg";

        private ComponentData _Data = new ComponentData();

        private Dictionary<Type, Editor> _EditorMap
             = new Dictionary<Type, Editor>();
        #endregion Private Variables

        #region Public Methods
        public void Clear()
        {
            _Data.Clear();
            _EditorMap.Clear();
        }

        public Editor GetTypeEditor(Type messageType)
        {
            if(!_EditorMap.ContainsKey(messageType))
            {
                var editor = CreateMessageEditor(messageType);
                _EditorMap.Add(messageType, editor);
            }

            return _EditorMap[messageType];
        }
        public object GetMessage(Type messageType)
        {
            var target = _EditorMap[messageType].target;
            var field  = target.GetType().GetField(_FieldName, BindingFlags.Public | BindingFlags.Instance);

            return field.GetValue(target);
        }

        public void SetMessage(Type messageType, object message)
        {
            var target = _EditorMap[messageType].target;

            var field = target.GetType().GetField(_FieldName, BindingFlags.Public | BindingFlags.Instance);
            field.SetValue(target, message);
        }

        public void CreateComponentData(GameObject obj)
        {
            _Data.Clear();

            var components = obj.GetComponents<Component>();
            foreach(var component in components)
            {
                if(component == null)
                {
                    continue;
                }
                var methodList = MessageDispatcher.QueryCache(component);
                if(methodList == null)
                {
                    continue;
                }

                List<PerMethodData> methodData = new List<PerMethodData>();
                foreach(var method in methodList)
                {
                    PerMethodData perMethodData = new PerMethodData();
                    perMethodData.Show = false;
                    perMethodData.MethodData = method;

                    methodData.Add(perMethodData);
                }

                PerComponentData perComponentData = new PerComponentData();
                perComponentData.Component = component;
                perComponentData.Show = false;
                perComponentData.MethodData = methodData;
                _Data.Add(component, perComponentData);
            }
        }
        public bool Empty()
        {
            return _Data == null || _Data.Count == 0;
        }
        #endregion Public Methods

        #region Private Methods
        private static Editor CreateMessageEditor(Type messageType)
        {
            var type =  CompileResultType(messageType);
            var instance = ScriptableObject.CreateInstance(type);

            return Editor.CreateEditor(instance);
        }
        private static Type CompileResultType(Type message)
        {
            var assemblyName = new AssemblyName(_AssemblyDllName);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder     = assemblyBuilder.DefineDynamicModule(assemblyName.Name);

            TypeBuilder tb = moduleBuilder.DefineType(_ClassDllName,
                    TypeAttributes.Public |
                    TypeAttributes.Class |
                    TypeAttributes.AutoClass |
                    TypeAttributes.AnsiClass |
                    TypeAttributes.BeforeFieldInit |
                    TypeAttributes.AutoLayout |
                    TypeAttributes.NestedFamANDAssem |
                    TypeAttributes.NestedPublic |
                    TypeAttributes.Serializable,
                    typeof(ScriptableObject));

            ConstructorBuilder constructor = tb.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);

            tb.DefineField(_FieldName, message, FieldAttributes.Public);

            return tb.CreateType();
        }
        #endregion Private Methods
    }
}