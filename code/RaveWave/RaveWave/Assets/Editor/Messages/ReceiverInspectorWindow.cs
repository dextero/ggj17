﻿namespace Moonlit.Messages.Tools
{
    using System;

    using System.Reflection;
    using System.Collections.Generic;

    using UnityEditor;
    using UnityEngine;

    public class ReceiverInspectorWindow : EditorWindow
    {
        #region Private Variables
        private static string _WindowName     = "Receivers";

        private MessageInspector _Controller = new MessageInspector();
        private GUIStyle _Style = new GUIStyle(EditorStyles.helpBox);
        #endregion Private Variables

        #region Unity Messages
        [MenuItem("Window/MessageReceiver Inspector")]
        public static void ShowWindow()
        {
            var window = GetWindow(typeof(ReceiverInspectorWindow)) as ReceiverInspectorWindow;
            window.titleContent = new GUIContent(_WindowName);
            window.autoRepaintOnSceneChange = true;

            window.Show();
            window.Refresh();
        }

        private void OnGUI()
        {
            if(_Controller.Empty())
            {
                EditorGUILayout.LabelField("Select object with components receiving messages");
                return;
            }
            EditorGUILayout.Space();
            DrawMethods();
            EditorGUILayout.Space();
        }

        private void OnHierarchyChange()
        {
            Show();
            Refresh();
        }
        private void OnSelectionChange()
        {
            Refresh();
        }
        #endregion Unity Messages

        #region Private Methods
        private void Refresh()
        {
            var selection = Selection.gameObjects;
            if(selection.Length == 1)
            {
                _Controller.Clear();
                _Controller.CreateComponentData(selection[0]);
            }

            Repaint();
        }

        private void DrawMethods()
        {
            foreach(var componentData in _Controller.Data)
            {
                EditorGUI.indentLevel = 0;
                EditorGUILayout.BeginVertical(_Style);

                var component        = componentData.Key;
                var perComponentData = componentData.Value;

                var componentTypeName = component.GetType().Name;

                if(perComponentData.Show = EditorGUILayout.Foldout(perComponentData.Show, componentTypeName))
                {
                    foreach(var perMethodData in perComponentData.MethodData)
                    {
                        EditorGUILayout.BeginVertical(_Style);
                        EditorGUI.indentLevel = 1;

                        var displayName    = ComposeDisplayName(perMethodData);
                        if(perMethodData.Show = EditorGUILayout.Foldout(perMethodData.Show, displayName))
                        {
                            DrawComponentMethod(componentData.Value, perMethodData);
                        }
                        EditorGUILayout.EndVertical();
                    }
                }

                EditorGUILayout.EndVertical();
            }
        }
        private void DrawComponentMethod(MessageInspector.PerComponentData componentData, MessageInspector.PerMethodData perMethodData)
        {
            EditorGUI.indentLevel = 2;
            {
                var messageType = perMethodData.MethodData.MessageType;
                var editor = _Controller.GetTypeEditor(messageType);
                editor.DrawDefaultInspector();

                if(GUILayout.Button("Send"))
                {
                    var value = _Controller.GetMessage(messageType);

                    MessageDispatcher.Register(componentData.Component); // [todo] change to bind
                    MessageDispatcher.DirectSignal(value, componentData.Component);
                    MessageDispatcher.Unregister(componentData.Component);
                }

                EditorGUILayout.Space();
            }
        }
        #endregion Private Methods

        #region Helper Methods
        private string ComposeDisplayName(MessageInspector.PerMethodData methodData)
        {
            var methodName  = methodData.MethodName;
            var messageName = methodData.MessageTypeName;

            var dispatchName = "";

            switch(methodData.Dispatch)
            {
                case DispatchMethod.Observer: dispatchName   = " : Observer"; break;
                case DispatchMethod.Subscriber: dispatchName = " : Subscriber"; break;
            }

            return methodName + "(" + messageName + ")" + dispatchName;
        }
        #endregion Helper Methods
    }
}